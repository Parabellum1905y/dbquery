package test;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (/*schema="webfiles", */name="webfiles")
public class MediaFile {
	@Id
	@GeneratedValue(generator="increment", strategy=GenerationType.IDENTITY)
	Long fileId = -1L;
	String name = "";
	String realName = "";
	Long size = -1L;
	int mediaFormat = -1;
	int isDirectory = -1;
	Long parentId = -1L;
	Date createDate = new Date ();
	int depth = -1;
	int hidden = -1;
	int readonly = -1;
	int state = -1;
	
	public Long getFileID() {
		return fileId;
	}

	public void setFileID(Long fileID) {
		fileId = fileID;
	}
	
	public Long getSize() {
		return size;
	}

	public void setSize(Long Size) {
		size = Size;
	}
	
	public int getMediaFormat() {
		return mediaFormat;
	}

	public void setMediaFormat(int MediaFormat) {
		mediaFormat = MediaFormat;
	}
	
	public int getIsDirectory() {
		return isDirectory;
	}

	public void setIsDirectory(int IsDirectory) {
		isDirectory = IsDirectory;
	}

	public Long getpParentId() {
		return parentId;
	}

	public void setParentId(Long ParentId) {
		parentId = ParentId;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getRealName() {
		return realName;
	}

	public void setRealName(String RealName) {
		this.realName = RealName;
	}
	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	public int getDepth() {
		return depth;
	}

	public void setDepth(int Depth) {
		depth = Depth;
	}
	
	public int getHidden() {
		return hidden;
	}

	public void setHidden(int Hidden) {
		hidden = Hidden;
	}
	
	public int getReadonly() {
		return readonly;
	}

	public void setReadonly(int Readonly) {
		readonly = Readonly;
	}
	
	public int getState() {
		return state;
	}

	public void setState(int State) {
		state = State;
	}
	
}

