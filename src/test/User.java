package test;


import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.owlike.genson.Genson;

@Entity
@Table (name="USERS")
public class User {
	@Id
	@GeneratedValue(generator="increment", strategy=GenerationType.IDENTITY)
	Long UserId = -1L;
	String userLogin;
	String userPass;
	String userMail;
	Integer enabled;
	Integer[] simpleArray;
	@Transient
	String lang;
	@OneToOne
	@JoinTable(name = "User_Details", 
				joinColumns=@JoinColumn(name="UserID", referencedColumnName="UserID"))
	UserDetails userDetails;
	@OneToMany
	@JoinTable(name = "User_Log", 
				joinColumns=@JoinColumn(name="UserID", referencedColumnName="UserID"))
	List<UserLog> userLog;

	public User() {}

	public Long getUserId() {
		return UserId;
	}

	public String getUserLogin() {
		return userLogin;
	}

	public String getUserPass() {
		return userPass;
	}

	public String getLang() {
		return lang;
	}

	public void setUserId(Long id) {
		this.UserId = id;
	}

	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}

	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getUserMail() {
		return userMail;
	}

	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Integer[] getSimpleArray() {
		return simpleArray;
	}

	public void setSimpleArray(Integer[] simpleArray) {
		this.simpleArray = simpleArray;
	}
	
	@Override
	public String toString() {
		String serRet = "";
		serRet = new Genson().serialize(this);
		return serRet;
	}
}
