package test;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table (name="USER_LOG")
public class UserLog {
	@Id
	Long logId = -1L;
	Long userId;
	String userIp;
	String sessionId;
	Integer logaction;
	Date rowDate;
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getUserIp() {
		return userIp;
	}
	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public Integer getLogaction() {
		return logaction;
	}
	public void setLogaction(Integer logaction) {
		this.logaction = logaction;
	}
	public Date getRowDate() {
		return rowDate;
	}
	public void setRowDate(Date rowDate) {
		this.rowDate = rowDate;
	}
	public Long getLogId() {
		return logId;
	}
	public void setLogId(Long logId) {
		this.logId = logId;
	}
	
}
