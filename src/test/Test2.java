package test;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arashan.dbconnect.jdbctools.DBconnector;
import com.arashan.dbconnect.jdbctools.WFDBconnector;
import com.arashan.tools.collections.ImmutableMap;

public class Test2 {
	
	public static String hash(String password) {
	    try {
	        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
	        String salt = "random_salt";
	        String passWithSalt = password + salt;
	        byte[] passBytes = passWithSalt.getBytes();
	        byte[] passHash = sha256.digest(passBytes);             
	        StringBuilder sb = new StringBuilder();
	        for(int i=0; i< passHash.length ;i++) {
	            sb.append(Integer.toString((passHash[i] & 0xff) + 0x100, 16).substring(1));         
	        }
	        String generatedPassword = sb.toString();
	        return generatedPassword;
	    } catch (NoSuchAlgorithmException e) { e.printStackTrace(); }       
	    return null;
	}

	private static final Logger logger = LoggerFactory.getLogger(Test2.class);
	public static void  main (String args[]){
		InitApp ia = new InitApp();
		ia.init();
		long t = System.currentTimeMillis();
		String [] names = {"John", "Foma", "Miha", "Mafusail", "Поликарп", "Акакий", "Олег", "Авдотья", "Карина", 
				"Ефросинья", "Варвара", "Агафья", "Helen", "Sulamif", "Sofia"};
		String [] nicknames = {"Smith", "Fagot", "Balbes", "Junk", "Master", "Няшечка^^", "Ололошенька", "Воген", "Biblbroks", 
				"Grot", "Grek", "00ooeee", "H^1$@1", "Idiot1995", "Shkolota"};
		String [] surnames = {"Помоев", "Заборов", "Поленьев", "Махоркин", "Rubinstein", "Richard", "Агафонов", "ЫЫЫяяя", "ОООнеет", 
				"Елизарьев", "Эгегей", "Fromfield", "Ololoi", "Ололоев", "Блаблаблаев"};
		Map<Integer, String> m = ImmutableMap.of(1, "А", 2, "A", 3, "Б", 4, "B", 5, "В", 6, "V", 7, "Г", 8, "G",
				9, "Д", 10, "D", 11, "Е", 12, "E", 13, "Ё", 14, "E", 15, "Ж", 16, "J", 17, "З", 18, "Z", 19, "И", 20, "I",
				21, "Й", 22, "I", 23, "К", 24, "K", 25, "Л", 26, "L", 27, "М", 28, "M", 29, "Н", 30, "N", 31, "О", 32, "O",
				33, "П", 34, "P", 35, "Р", 36, "R", 37, "С", 38, "S", 39, "Т", 40, "T", 41, "У", 42, "U", 43, "Ф", 44, "F",
				45, "Х", 46, "H", 47, "Ч", 48, "Ch", 49, "Ц", 50, "Ts",
				51, "Ш", 52, "Sh", 53, "Щ", 54, "Sh", 55, "Ъ", 56, "Ы", 57, "Y", 58, "Ь",  59, "Э", 60, "E", 61, "Ю", 62, "Iy", 
				63, "Я", 64, "Ya",
				65, "а", 66, "a", 67, "б", 68, "b", 69, "в", 70, "v", 71, "г", 0, "g");
		
		Date curDate = new Date ();
		for (int i = 0; i < 100000; i ++){
			DBconnector connector = WFDBconnector.getPermConnection("java:comp/env/jdbc/USERS_INFO");
			if (connector.startTransaction()){
				for (int j = 0; j < 80; j ++){
					User ff = new User();
					ff.setEnabled(1);
					ff.setLang("RU");
					ff.setUserLogin("krasava2"+UUID.randomUUID());
					String passwd =hash("qwerty"); 
					ff.setUserPass(passwd);
					ff.setUserMail("azazello369@gmail.com");
					Integer [] array = {1, 2, 3};
					ff.setSimpleArray(array);
					
					UserDetails ud = new UserDetails ();
					Random rnd = new Random();
					
					ud.setSurname(surnames [rnd.nextInt(names.length)]);
					ud.setName(names [rnd.nextInt(names.length)]);
					ud.setNick(nicknames [rnd.nextInt(names.length)]+rnd.nextInt(10000)+m.get(rnd.nextInt(71))+rnd.nextInt(10000)+m.get(rnd.nextInt(71)));
					ud.setRegisterDate (curDate);
					
					try {
						User getf = connector.insert(ff);
						if (getf.getUserId()!=-1L){
							ud.setUserID(getf.getUserId());
							connector.insert(ud);
						}
					} catch (IllegalArgumentException | IllegalAccessException
							| SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				connector.endTransaction();
			}
			connector.closePermConnection();
		}
		logger.info("finished!!!");
		try{
			Thread.sleep (20000);
		}catch(Exception e){}
		DBconnector connector = WFDBconnector.getPermConnection("java:comp/env/jdbc/USERS_INFO");
		User ff = new User();
		ff.setEnabled(1);
		ff.setLang("RU");
		ff.setUserLogin("krasava2"+UUID.randomUUID());
		ff.setUserPass("qwerty");
		ff.setUserMail("azazello369@gmail.com");
		try {
			logger.info("join:{}", connector.getListJoin(User.class, " where USERS.UserId=?", Arrays.asList(39L), Arrays.asList(UserDetails.class)));
			ff = (User)connector.insert(ff);
			logger.info ("{}", ff);
		} catch (Exception e) {
			logger.error ("{}",e);
		}finally {
			connector.closePermConnection();
			t = System.currentTimeMillis()-t;
			logger.info("time used:"+t);
		}
		
	}
}
