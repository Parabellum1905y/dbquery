package test;

import java.beans.PropertyVetoException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;
import javax.naming.spi.InitialContextFactoryBuilder;
import javax.naming.spi.NamingManager;

import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arashan.dbconnect.jdbctools.AppEntitiesConteiner;
import com.arashan.dbconnect.jdbctools.listeners.AppSimpleUtil;
import com.arashan.system.config.Config;
import com.arashan.system.config.LoadConfig;
import com.mchange.v2.c3p0.ComboPooledDataSource;
class DbInitConfig {
	public String URL = "";
	public String JNDI_NAME = "";
	public String PWD = "";
	public String USER = "";
	public String DRIVER = "";
	/**
	 * db pooling parameters
	 */
	public Integer MIN_POOL_SIZE=5;
	public Integer MAX_POOL_SIZE=20;
	public Integer ACQUIREINCREMENT=10;
	public Integer MAX_STATEMENTS=1000;
	public String CHECK_QUERY = "";
}

public class InitApp {
	private final Logger logger = LoggerFactory.getLogger(InitApp.class);
	Map<String, DbInitConfig> dbConfigs = new HashMap<String, DbInitConfig>();
	/**
	 * database connection parameters
	 */
	
	public void init (){
		Config conf = LoadConfig.loadConfig("main/resources/jndi.properties");
		logger.info("{}", conf);
		if (conf != null){
			if (conf.getProp("pools") !=null){
				String pools []= conf.getProp("pools").split("\\,");
				for (String pool:pools){
					DbInitConfig dbConf = new DbInitConfig();
					if (conf.getProp(pool+".JNDI_NAME") != null){
						dbConf.JNDI_NAME = conf.getProp(pool+".JNDI_NAME");
					}
					if (conf.getProp(pool+".URL") != null){
						dbConf.URL = conf.getProp(pool+".URL");
					}
					if (conf.getProp(pool+".PWD") != null){
						dbConf.PWD = conf.getProp(pool+".PWD");
					}
					if (conf.getProp(pool+".USER") != null){
						dbConf.USER = conf.getProp(pool+".USER");
					}
					if (conf.getProp(pool+".MIN_POOL_SIZE") != null){
						dbConf.MIN_POOL_SIZE = Integer.parseInt(conf.getProp(pool+".MIN_POOL_SIZE"));
					}
					if (conf.getProp(pool+".MAX_POOL_SIZE") != null){
						dbConf.MAX_POOL_SIZE = Integer.parseInt(conf.getProp(pool+".MAX_POOL_SIZE"));
					}
					if (conf.getProp(pool+".ACQUIREINCREMENT") != null){
						dbConf.ACQUIREINCREMENT = Integer.parseInt(conf.getProp(pool+".ACQUIREINCREMENT"));
					}
					if (conf.getProp(pool+".MAX_STATEMENTS") != null){
						dbConf.MAX_STATEMENTS = Integer.parseInt(conf.getProp(pool+".MAX_STATEMENTS"));
					}
					if (conf.getProp(pool+".CHECK_QUERY") != null){
						dbConf.CHECK_QUERY = conf.getProp(pool+".CHECK_QUERY");
					}
					if (conf.getProp(pool+".DRIVER") != null){
						dbConf.DRIVER = conf.getProp(pool+".DRIVER");
					}
					dbConfigs.put(dbConf.JNDI_NAME, dbConf);
				}
			}
		}
		setupInitialContext ();
    	AppEntitiesConteiner aec = new AppEntitiesConteiner ();
    	AppSimpleUtil.setAppEntitiesConteiner(aec);
//		AppGroupsStaticObjects.updateGroupRelAll();
	}

	private void setupInitialContext() {
	    try {
	        NamingManager.setInitialContextFactoryBuilder(new InitialContextFactoryBuilder() {

	            @Override
	            public InitialContextFactory createInitialContextFactory(Hashtable<?, ?> environment) throws NamingException {
	                return new InitialContextFactory() {

	                    @Override
	                    public Context getInitialContext(Hashtable<?, ?> environment) throws NamingException {
	                        return new InitialContext(){

	                            private Hashtable<String, javax.sql.DataSource> dataSources = new Hashtable<>();

	                            @Override
	                            public Object lookup(String name) throws NamingException {

	                                if (dataSources.isEmpty()) { //init datasources
//	                                    for (String key:dbConfigs.keySet()){
//		                                    ComboPooledDataSource cpds = new ComboPooledDataSource();
//		                                    DbInitConfig dbConf = dbConfigs.get(key);
//		                                    try {
//												cpds.setDriverClass(dbConf.DRIVER);
//			                                    cpds.setJdbcUrl(dbConf.URL);
//			                                    cpds.setUser(dbConf.USER);
//			                                    cpds.setPassword(dbConf.PWD);
//			                                    cpds.setMinPoolSize(dbConf.MIN_POOL_SIZE);
//			                                    cpds.setAcquireIncrement(dbConf.ACQUIREINCREMENT);
//			                                    cpds.setMaxPoolSize(dbConf.MAX_POOL_SIZE);
//			                                    cpds.setMaxStatements(dbConf.MAX_STATEMENTS);
//											} catch (PropertyVetoException e) {
//												logger.error("exception:",e);
//											}
//		                                    dataSources.put(dbConf.JNDI_NAME, cpds);
//	                                    }
	                                    for (String key:dbConfigs.keySet()){
	                                    	BasicDataSource cpds = new BasicDataSource();
		                                    DbInitConfig dbConf = dbConfigs.get(key);
		                                	try {
												Class.forName(dbConf.DRIVER);
											} catch (ClassNotFoundException e) {
												logger.error("exception:",e);
											}
		                                	logger.debug("driver className {}", dbConf.DRIVER);
											cpds.setDriverClassName( dbConf.DRIVER);
		                                    cpds.setUrl(dbConf.URL);
		                                    cpds.setUsername(dbConf.USER);
		                                    cpds.setPassword(dbConf.PWD);
		                                    cpds.setMinIdle(dbConf.MIN_POOL_SIZE);
		                                    cpds.setValidationQuery(dbConf.CHECK_QUERY);
		                                    cpds.setMaxTotal(dbConf.MAX_POOL_SIZE);
		                                    cpds.setMaxOpenPreparedStatements(dbConf.MAX_STATEMENTS);
		                                    dataSources.put(dbConf.JNDI_NAME, cpds);
	                                    }
	                                }

	                                if (dataSources.containsKey(name)) {
	                                    return dataSources.get(name);
	                                }

	                                throw new NamingException("Unable to find datasource: "+name);
	                            }
	                        };
	                    }

	                };
	            }

	        });
	    }
	    catch (NamingException ne) {
	        ne.printStackTrace();
	    }
	}
}
