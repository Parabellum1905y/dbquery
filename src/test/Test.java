package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arashan.dbconnect.jdbctools.DBconnector;
import com.arashan.dbconnect.jdbctools.WFDBconnector;

public class Test {
	private static final Logger logger = LoggerFactory.getLogger(Test.class);
	public static void  main (String args[]){
		InitApp ia = new InitApp();
		ia.init();
		long t = System.currentTimeMillis();
		DBconnector connector = WFDBconnector.getPermConnection("java:comp/env/jdbc/USERS_INFO");
		User ff = new User();
		ff.setEnabled(1);
		ff.setLang("RU");
		ff.setUserLogin("krasava2"+UUID.randomUUID());
		ff.setUserPass("qwerty");
		ff.setUserMail("azazello369@gmail.com");
		Integer [] array = {1, 2, 3};
		ff.setSimpleArray(array);
		logger.info ("{}", ff);
		try {
			logger.info("join:{}", connector.getListJoin(User.class, " where USERS.UserId=?", 
						Arrays.asList(39L), Arrays.asList(UserDetails.class)));
			logger.info ("{}", connector.getObjectJoin(User.class, "where userLogin=?", 
						Arrays.asList((Object)"krendel"), new ArrayList<String>(), Arrays.asList(UserLog.class)));
//			ff = (User)connector.insert(ff);
			logger.info ("{}", ff);
			ff.setUserPass("111");
//			logger.info ("{}", connector.update(ff));
		} catch (Exception e) {
			logger.error ("{}",e);
		}finally {
			connector.closePermConnection();
			t = System.currentTimeMillis()-t;
			logger.info("time used:"+t);
		}
		
	}
}
