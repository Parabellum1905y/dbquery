package test;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * Service for Time Capsule
 * @author luger
 *
 */
@Entity
@Table (name="TIMECAPSULE_SERVICE")
public class TimeCapsuleService {

	@Id
	@GeneratedValue(generator="increment", strategy=GenerationType.IDENTITY)
	protected Long rowId = -1L;
	@Transient
	protected User creator;
	protected Long creatorId;
	protected Date createDate;
	protected Long []tags = {};// --only size=16
	/**
	 * 
	 */
	@Transient
	private static final long serialVersionUID = -3766153373930093192L;
	Date openDate;
	Long []consumers = {};
	Long []consumerGroups = {};
	String theme;
	String message;
	Long []relatedFileLinks = {};
	@Transient
	String place;
	
	public Date getOpenDate() {
		return openDate;
	}
	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}
	public Long[] getConsumers() {
		return consumers;
	}
	public void setConsumers(Long[] consumers) {
		this.consumers = consumers;
	}
	public String getTheme() {
		return theme;
	}
	public void setTheme(String theme) {
		this.theme = theme;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Long[] getRelatedFileLinks() {
		return relatedFileLinks;
	}
	public void setRelatedFileLinks(Long[] relatedFileLinks) {
		this.relatedFileLinks = relatedFileLinks;
	}
	public Long[] getConsumerGroups() {
		return consumerGroups;
	}
	public void setConsumerGroups(Long[] consumerGroups) {
		this.consumerGroups = consumerGroups;
	}

	public User getCreator() {
		return creator;
	}
	public void setCreator(User creator) {
		this.creator = creator;
	}
	public Long getCreatorId() {
		return creatorId;
	}
	public void setCreatorId(Long creatorId) {
		this.creatorId = creatorId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Long getRowId() {
		return rowId;
	}
	public void setRowId(Long rowId) {
		this.rowId = rowId;
	}
	public Long[] getTags() {
		return tags;
	}
	public void setTags(Long[] tags) {
		this.tags = tags;
	}
}
