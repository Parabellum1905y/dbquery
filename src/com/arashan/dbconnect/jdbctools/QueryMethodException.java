package com.arashan.dbconnect.jdbctools;

/**
 * class-exception for error's signals in forming SQL queries
 * @author luger
 *
 */
public class QueryMethodException extends IllegalArgumentException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3105993048545860536L;
	
	public QueryMethodException (){
		super();
	}
	
	public QueryMethodException(String s){
		super(s);
	}
	
	public QueryMethodException(String s, Throwable cause){
		super (s, cause);
	}

	public QueryMethodException(Throwable cause){
		super (cause);
	}
}
