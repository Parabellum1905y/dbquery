package com.arashan.dbconnect.jdbctools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class FieldWhere{
	public List<String>tableSeq = new ArrayList<String>();
	public String field;
}

public class WhereBuilder {
	private final Logger logger = LoggerFactory.getLogger(WhereBuilder.class);
	IWhere where = null;
	String mainClass = "";
	String mainTable = "";
	List<On>joinBuilders = null;
	
	public WhereBuilder(IWhere where, List<On> joinBuilders, String mainClass, String mainTable){
		this.where = where;
		this.joinBuilders = joinBuilders;
		this.mainClass = mainClass;
		this.mainTable = mainTable;
	}

	public String build (){
		if (where == null) return "";
		List<Operand> ops = where.getStack();
		StringBuffer stBuf = new StringBuffer();
		stBuf.append(" WHERE ");
		ops.forEach(op->{
			boolean isOperator = op.getMode() == 1;
			if (isOperator){
				switch(op.getCond()){
					case lb:
						stBuf.append("(");
						break;
					case rb:
						stBuf.append(")");
						break;
					case and:
						stBuf.append(" AND ");
						break;
					case or:
						stBuf.append(" OR ");
						break;
				}
			}else{
				WhereClause c = op.getWc();
				String reorgField = formRealStringWhereField(analizeFieldString(c.getField()));
				switch (c.getMode()){
				case eq:
					stBuf.append(" "+reorgField+"=? ");
					break;
				case ne:
					stBuf.append(" "+reorgField+"<>? ");
					break;
				}
			}
		});
		return stBuf.toString();
	}
	
	private FieldWhere analizeFieldString (String fieldWhere){
		if (fieldWhere.isEmpty()) return null;
		FieldWhere fw = new FieldWhere();
		String[] tables = fieldWhere.split(Pattern.quote("."));
		int tl = tables.length;
		logger.info("fieldWhere:{}", fieldWhere);
		if (tl<2) throw new IllegalArgumentException("incorrect string : doesnt match field name");
		if (tl == 2) {
			fw.tableSeq = Arrays.asList(tables[0]);
			fw.field = tables[1];
		}else{
			fw.tableSeq = Arrays.asList(Arrays.copyOfRange(tables, 0, tl-1));
			fw.field = tables[tl-1];
		}
//		logger.info("seq:{}, field:{}, src:{}", fw.tableSeq, fw.field, fieldWhere);
		return fw;
	}
	
	private String formRealStringWhereField (FieldWhere fWhere){
		String ret = "";
		List<String> seq = fWhere.tableSeq;
		boolean ismain = seq.size() == 1;
		String prevRef = "";
		for (String t:seq){
//			logger.info("t::{}, mainClass::{}, mainTable ::{}", t, mainClass, mainTable);
			if (ismain && t.equals(mainClass)){
				ret = mainTable+"."+fWhere.field;
				break;
			}
			String tName = "";
			String jField = "";
			if (t.indexOf("'")!=-1){
				String [] tf = t.split("\\'");
				tName = tf[0];
				jField = tf[1];
			}else tName = t;
			int i = 0;
			for (On on:joinBuilders){
				IQueryBuilder qb = on.qBuilder;
				String joinTbName = qb.getTbName();
				String relFieldName = on.relativeFieldName;
				String className = qb.getOwnClassName();
//				logger.info("cn:{},tn:{}", className, tName);
				if (tName.equals(className)){
//					logger.info("s:{},{},{}", prevRef, relFieldName, joinTbName);
					if (!prevRef.isEmpty() && prevRef.equals(relFieldName)){
						ret = joinTbName+i+"."+fWhere.field;
						break;
					} else if (prevRef.isEmpty()){
						ret = joinTbName+i+"_"+fWhere.field;
						break;
					}
				}
				++ i;
			}
			prevRef = jField;
		}
		return ret;
	}
}
