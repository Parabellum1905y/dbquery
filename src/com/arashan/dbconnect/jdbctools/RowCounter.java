package com.arashan.dbconnect.jdbctools;

import javax.persistence.Entity;

@Entity
public class RowCounter {
	Long __ROWS_COUNT;

	public Long get__ROWS_COUNT() {
		return __ROWS_COUNT;
	}

	public void set__ROWS_COUNT(Long __ROWS_COUNT) {
		this.__ROWS_COUNT = __ROWS_COUNT;
	}
	
}
