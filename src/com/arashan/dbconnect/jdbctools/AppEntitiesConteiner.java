package com.arashan.dbconnect.jdbctools;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.persistence.JoinColumn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * class for storing 
 * Class fields and annotations
 * for automatic mapping
 * tables to beans
 * @author luger
 *
 */
public class AppEntitiesConteiner {
	private final Logger logger = LoggerFactory.getLogger(AppEntitiesConteiner.class);
	Map<String,EntityContainer> entities = new ConcurrentHashMap<String,EntityContainer>();
	
	public AppEntitiesConteiner (){
		logger.info("RowCounter loading status:"+addEntity(RowCounter.class));
	}
	
	public boolean addEntity (Class<?> c){
		logger.info("input class {}", c);
		String name = c.getCanonicalName();
		if (entities.get(name)!=null)return true;
		java.lang.annotation.Annotation ans []= c.getAnnotations();
		if (ans.length == 0) return false;
		boolean isEntity = false;
		String tableName = "";
		String schema = "";
		for (java.lang.annotation.Annotation an:ans){
			if (an instanceof javax.persistence.Entity){
				isEntity = true;
			}
			if (an instanceof javax.persistence.Table){
				tableName = ((javax.persistence.Table) an).name();
				schema = ((javax.persistence.Table) an).schema();
			}
			if (!tableName.isEmpty() && isEntity) break;
		}
		java.lang.reflect.Field [] fields = c.getDeclaredFields();
		if (fields.length == 0) return false;
		List<String> fieldNames = new ArrayList<String>();
		boolean isIdent = false;
		boolean autoGen = false;
		String identField = "";
		String currentJointField = "";
		Map<String, Map<Class<?>, Join>> joinFields = new ConcurrentHashMap<String, Map<Class<?>, Join>>(); 
		java.lang.reflect.Field ifield = null;
		List<java.lang.reflect.Field> fieldsList = new ArrayList<java.lang.reflect.Field>();
		List<java.lang.reflect.Field> joinFieldsList = new ArrayList<java.lang.reflect.Field>();
		
		try {
			for (java.lang.reflect.Field fi:fields){
				fi.setAccessible(true);
				java.lang.annotation.Annotation ansf [] = fi.getAnnotations();
				boolean isOneToMany = false;
				boolean isOneToOne = false;
				boolean isManyToOne = false;
				boolean isTransient = false;
				for (java.lang.annotation.Annotation an:ansf){
					if (an instanceof javax.persistence.Transient){
						isTransient = true;
						break;
					}
					if (an instanceof javax.persistence.Id){
						isIdent = true;
						identField = fi.getName().toUpperCase();
						ifield = fi;
					}
					if (an instanceof javax.persistence.GeneratedValue){
						autoGen = true;
					}
					if (an instanceof javax.persistence.OneToMany){
						isOneToMany = true;
						currentJointField = fi.getName().toUpperCase();
					}
					if (an instanceof javax.persistence.OneToOne){
						isOneToOne = true;
						currentJointField = fi.getName().toUpperCase();
					}
					if (an instanceof javax.persistence.ManyToOne){
						isManyToOne = true;
						currentJointField = fi.getName().toUpperCase();
					}
					// joins
					if (an instanceof javax.persistence.JoinTable){
						Class<?> cc = fi.getType();
						if (fi.getType().getName().contains("List")){
							ParameterizedType pt = (ParameterizedType) fi.getGenericType();
							cc = (Class<?>)pt.getActualTypeArguments()[0];
							this.addEntity(cc);
						}else{
							this.addEntity(fi.getType());
						}
						if (currentJointField.equals(fi.getName().toUpperCase())){
							if (isOneToMany || isOneToOne || isManyToOne){
								Join jField = new Join();
								JoinColumn[] jc = ((javax.persistence.JoinTable) an).joinColumns();
								String refName = "";
								String pkName = ""; 
								for (JoinColumn j:jc){
									refName = j.referencedColumnName();
									pkName = j.name();
									if (!refName.isEmpty() && !pkName.isEmpty()) break;
								}
								if (!refName.isEmpty()){
									jField.setTb(((javax.persistence.JoinTable) an).name());
									jField.setPk(pkName.isEmpty()?fi.getName().toUpperCase():pkName);
									jField.setFk(refName);
									jField.setRelativeFieldName(fi.getName());
									if (isOneToMany) jField.setJoinMode(1);
									if (isOneToOne) jField.setJoinMode(2);
									if (isManyToOne) jField.setJoinMode(3);
									Map<Class<?>, Join> m = new HashMap<>();
									m.put(cc, jField);
									joinFields.put(fi.getName(), m);
								}
							}
						}
					}
				}//end for annotations
				if (!isOneToMany && !isOneToOne && !isTransient){
					fieldsList.add(fi);
					fieldNames.add(fi.getName());
				}
				if (isOneToOne || isOneToMany){
					joinFieldsList.add(fi);
				}
//				if (!isOneToMany && !isOneToOne)fieldNames.add(fi.getName());
			}
		}catch (Exception ee){logger.info("error:{}", ee);}
//		if (!isIdent) return false;
		EntityContainer ec = new EntityContainer();
		ec.setAnn(ans);
		ec.setFields(fieldsList);
		ec.setJoinFieldList(joinFieldsList);
		ec.setFieldNames(fieldNames);
		ec.setIsEntity(isEntity);
		ec.setHasIdent(isIdent);
		ec.setSchema(schema);
		ec.setTableName(tableName);
		ec.setIdentField(ifield);
		ec.setIdentIsAutoGenerated(autoGen);
		ec.setIdentFieldName(identField);
		ec.setJoinFields(joinFields);
		
		this.entities.put(name, ec);
		return true;
	}
	
	public boolean removeEntity (Class<?> c){
		String name = c.getCanonicalName();
		if (entities.get(name)==null)return true;
		entities.remove(name);
		return true;
	}
	
	public EntityContainer getEntity (Class<?> c){
		String name = c.getCanonicalName();
		if (entities.get(name)==null)return null;
		return entities.get(name);
	}
}
