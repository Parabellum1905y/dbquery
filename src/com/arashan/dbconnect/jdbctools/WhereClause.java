package com.arashan.dbconnect.jdbctools;

enum ClauseType{
	eq,
	ne
}

public class WhereClause {
	/**
	 * 
	 */
	ClauseType mode = null;
	
	String field = "";
	Object eqObject = null;
	
	public static WhereClause w(){
		return new WhereClause();
	}
	
	public WhereClause eq (String field, Object eqObject){
		this.field = field;
		mode = ClauseType.eq;
		this.eqObject = eqObject;
		return this;
	}

	public WhereClause ne (String field, Object eqObject){
		this.field = field;
		mode = ClauseType.ne;
		this.eqObject = eqObject;
		return this;
	}

	public ClauseType getMode() {
		return mode;
	}

	public void setMode(ClauseType mode) {
		this.mode = mode;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public Object getEqObject() {
		return eqObject;
	}

	public void setEqObject(Object eqObject) {
		this.eqObject = eqObject;
	}
//	public WhereClause<T>
}