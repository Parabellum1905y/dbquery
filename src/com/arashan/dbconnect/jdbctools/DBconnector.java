package com.arashan.dbconnect.jdbctools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arashan.dbconnect.jdbctools.listeners.AppSimpleUtil;

/**
 * Big Class for creating connections To DB
 * Creating SQL Queries and perform those
 * 
 * @author luger
 *
 */
abstract public class DBconnector {
	private static final Logger logger = LoggerFactory.getLogger(DBconnector.class);
	protected String jndi_name = "";
	protected boolean constConnected = false;
	protected boolean useTransaction = false;
	protected int transactionState = 0;
	protected Savepoint rollbackLabel = null;
	protected Connection permCon = null;
	protected PreparedStatement tranps = null;
	protected PreparedStatement batchTranps = null;
	protected String batchQuery = "";
	protected ResultSetParser parser = new ResultSetParser ();

	protected List<Object> gkeys = new ArrayList <Object>();
	protected List<Map<String, Object>> lastSelectResult = new ArrayList<Map<String, Object>>();
//	protected List
	public DBconnector (){
		this.jndi_name = AppSimpleUtil.getGlobalJNDI();
	}

	public DBconnector (String jndi_name){
		this.jndi_name = jndi_name;
	}
	
	abstract protected Connection createConnection() throws SQLException;
	abstract protected void closeConnection (Connection conn);
	/**
	 * cosntructor for creating permanent
	 * connection
	 * @param constConnected
	 */
	protected DBconnector (boolean constConnected){
		this.constConnected = constConnected;
		this.jndi_name = AppSimpleUtil.getGlobalJNDI();
		logger.info("jndi_name:"+jndi_name);
		if (this.constConnected)
			try {
				permCon =  createConnection();
			} catch (SQLException e) {
				this.constConnected = false;
				permCon = null;
			}
	}

	protected DBconnector (boolean constConnected, String jndi_name){
		this.constConnected = constConnected;
		this.jndi_name = jndi_name;
		if (this.constConnected)
			try {
				permCon =  createConnection();
			} catch (SQLException e) {
				this.constConnected = false;
				permCon = null;
			}
	}

	public void closePermConnection (){
		try {
			if (batchTranps != null) {batchTranps.clearBatch(); batchTranps.close (); batchTranps = null;}
		} catch (Exception e) {}
		try {
			if (this.isUsingTransaction()) {this.endTransaction(true);}
		} catch (Exception e) {}
		if (this.constConnected){
			if (permCon != null){
				try{
					permCon.setAutoCommit(true);
				}catch (Exception e){}
			}
			if (permCon != null){
				closeConnection(permCon);
			}
		}
		permCon = null;
	}
	
	public boolean isPersistent (){
		return this.constConnected;
	}
	
	public boolean isUsingTransaction (){
		return this.useTransaction;
	}
	
	public boolean isAccessed (){
		return this.permCon!= null;
	}

	public boolean startTransaction () {
		logger.info("starting transaction");
		if (this.isPersistent() && !this.useTransaction){
			this.useTransaction = true;
			try {
				logger.info("trying to start...");
				this.permCon.setAutoCommit(false);
				this.rollbackLabel = this.permCon.setSavepoint("rollback");
				this.transactionState = 1; // started
				gkeys.clear();
				logger.info("transaction start has successed");
			} catch (SQLException e) {
				e.printStackTrace();
				logger.error("transaction start has failed:"+e.getMessage());
				this.useTransaction = false;
				this.transactionState = 0; // failed
			}
		}
		return this.useTransaction;
	}

	/**
	 * Adding Edit query to transaction
	 * @param query
	 * @param params
	 * @return
	 * @throws SQLException
	 */
	public int addToTransaction (String query, List<?> params) throws SQLException{
		return this.addToTransaction(query, params, "1L");
	}
	
	/**
	 * Adding Edit query to transaction
	 * @param query
	 * @param params
	 * @return
	 * @throws SQLException
	 */
	public int addToTransaction (String query, List<?> params, String primaryKey) throws SQLException{
		
		logger.info("add To transaction ; query: " + query+";params:"+params);
		int count = 0;
		if (constConnected && this.useTransaction){
			if (this.permCon != null){
				if (primaryKey.equals("1L")){
					tranps = this.permCon.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
				}else{
					tranps = this.permCon.prepareStatement(query,new String[]{primaryKey});
				}
				this.prepareParamStatement(this.permCon, tranps, params);
			}else {
				this.transactionState = 0; 
				throw new SQLException ("Connection is null");
			}
		}else{
			this.transactionState = 0; 
			throw new SQLException ("start transaction failed");
		}
		ResultSet rs = null;
		try {
			count =tranps.executeUpdate();
			rs = tranps.getGeneratedKeys();
			gkeys.clear();
			gkeys.addAll(parser.parseGeneratedKeys(rs));
//			while (rs != null && rs.next()) {
//				gkeys.add(rs.getString(1));
//			}
		}catch (Exception e){
			e.printStackTrace();
			this.transactionState = 0; // failed
		}finally {
			try {
				if (rs != null) rs.close();
				if (tranps != null) tranps.close ();
			} catch (Exception e) {}
		}
		return count;
	}

	/**
	 * Initialize Batch queries
	 * @param query
	 * @param primaryKey
	 * @return
	 */
	public boolean initBatch (String query, String primaryKey){
		boolean ret = false;
		this.batchQuery = query;
		try {
			if (batchTranps != null) {batchTranps.clearBatch(); batchTranps.close (); batchTranps = null;}
		} catch (Exception e) {}
		try{
			if (constConnected && this.useTransaction){
				if (this.permCon != null){
					if (primaryKey.equals("1L")){
						batchTranps = this.permCon.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
					}else{
						batchTranps = this.permCon.prepareStatement(query,new String[]{primaryKey});
					}
					if (batchTranps!=null) ret = true;
				}
			}else{
				this.transactionState = 0; 
				throw new SQLException ("init Batch failed");
			}
		}catch(SQLException sqe){
			logger.error("error in initBatch:cannot initialize Batch statement: "+sqe.getMessage());
			ret = false;
		}
		return ret;
	}
	/**
	 * Adding parameters to Batch Query
	 * @param query
	 * @param params
	 * @return
	 * @throws SQLException
	 */
	public boolean addToBatch (List<?> params) throws SQLException{
		boolean ret = false;
		logger.info("add To Batch;");
		if (constConnected && this.useTransaction && this.batchTranps!=null && !this.batchQuery.isEmpty()){
			this.prepareParamStatement(this.permCon, batchTranps, params);
			this.batchTranps.addBatch();
			ret = true;
		}else{
			this.transactionState = 0; 
			throw new SQLException ("add batch failed");
		}
		return ret;
	}
	
	public int [] executeBatch () throws SQLException{
		logger.info("execute Batch; query: " + batchQuery);
		int res[] = null;
		if (constConnected && this.useTransaction){
			if (this.permCon != null){
				ResultSet rs = null;
				try {
					res =batchTranps.executeBatch();
					rs = batchTranps.getGeneratedKeys();
					gkeys.clear();
					while (rs != null && rs.next()) {
						gkeys.add(rs.getString(1));
					}
				}catch (Exception e){
					e.printStackTrace();
					this.transactionState = 0; // failed
				}finally {
					try {
						if (rs != null) {rs.close();rs = null;}
						if (tranps != null) {tranps.close (); tranps = null;}
					} catch (Exception e) {}
				}
			}else {
				this.transactionState = 0; 
				throw new SQLException ("Connection is null");
			}
		}
		return res;
	}

	public long getTranactionState (){
		return this.transactionState;
	}
	
	public boolean endTransaction (){
		return this.endTransaction(false);
	}

	public boolean endTransaction (boolean needFail){
		boolean ans = false;
		if (this.isPersistent() && this.isUsingTransaction ()){
			try {
				if (batchTranps != null) {batchTranps.clearBatch(); batchTranps.close (); batchTranps = null;}
			} catch (Exception e) {}
			this.batchQuery = "";
			this.useTransaction = false;
			try{
				if (this.transactionState == 1 && !needFail){
					logger.info("transaction finished correctly");
					this.permCon.commit();
					ans = true;
				}else{
					logger.info("transaction finished incorrectly");
					this.permCon.rollback(this.rollbackLabel);
					this.permCon.releaseSavepoint(this.rollbackLabel);
					ans = false;
				}
			} catch (SQLException e) {
				ans = false;
				logger.error("error:{}",e);
			}finally{
				this.transactionState = 0; // finished
				try {
					this.permCon.setAutoCommit(true);
				} catch (SQLException e) {}
				try{
					if (tranps != null){tranps.close();tranps = null;}
				}catch(SQLException e){}
			}
		}else{
			try {
				this.permCon.setAutoCommit(true);
			} catch (SQLException e) {}
			try{
				if (tranps != null){tranps.close();tranps = null;}
			}catch(SQLException e){}
		}
		return ans;
	}
	/**
	 * returning generated keys for 1 transaction step
	 * @return
	 */
	public List<Object> getGKeys (){
		return this.gkeys;
	}

	/**
	 * Attention! Statement st isn't immutable object.
	 * @param st - prepared statement
	 * @param params - parameters for preparing
	 * @throws SQLException 
	 */
	protected void prepareParamStatement(Connection conn, PreparedStatement st, List<?> params) throws SQLException{
		for (int i = 0; i < params.size(); i ++){
			if (params.get(i) instanceof String){
				if (String.valueOf(params.get(i)).isEmpty()){
					st.setNull(i+1, java.sql.Types.VARCHAR);
				}else{
					st.setString(i+1, (String)params.get(i));
				}
			}else if (params.get(i)!=null && params.get(i).getClass().isArray()){
				Class<?> component = params.get(i).getClass().getComponentType();
				if (component.equals(String.class)){
					st.setArray(i+1, conn.createArrayOf("varchar", (Object[])params.get(i)));
				}else if (component.equals(Date.class)){
					st.setArray(i+1, conn.createArrayOf("timestamp", (Object[])params.get(i)));
				}else if (component.equals(Long.class)){
					st.setArray(i+1, conn.createArrayOf("bigint", (Object[])params.get(i)));
				}else if (component.equals(Float.class)){
					st.setArray(i+1, conn.createArrayOf("float8", (Object[])params.get(i)));
				}else if (component.equals(Integer.class)){
					st.setArray(i+1, conn.createArrayOf("int4", (Object[])params.get(i)));
				}else if (component.equals(Double.class)){
					st.setArray(i+1, conn.createArrayOf("float8", (Object[])params.get(i)));
				}else if (component.equals(BigDecimal.class)){
					st.setArray(i+1, conn.createArrayOf("numeric", (Object[])params.get(i)));
				}
			}else if (params.get(i) instanceof Date){
				st.setTimestamp(i+1, new java.sql.Timestamp(((java.util.Date)params.get(i)).getTime()));
			}else if (params.get(i) instanceof Long){
				st.setLong(i+1, (Long)params.get(i));
			}else if (params.get(i) instanceof Float){
				st.setFloat(i+1, (Float)params.get(i));
			}else if (params.get(i) instanceof Integer){
				st.setInt(i+1, (Integer)params.get(i));
			}else if (params.get(i) instanceof Double){
				st.setDouble(i+1, (Double)params.get(i));
			}else if (params.get(i) instanceof SQLXML){
				st.setSQLXML(i+1, (SQLXML) params.get(i));
			}else if (params.get(i) instanceof BigDecimal){
				st.setBigDecimal(i+1, (BigDecimal) params.get(i));
			}else if (params.get(i) == null){
				st.setNull(i+1,java.sql.Types.NULL);
			}
		}
	}

	/**
	 * old method for selecting any Data from DB
	 * 
	 * @param query
	 * @param params
	 * @return
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 */
	public List<Map<String, Object>> selectAnyFields (String query, List<?> params) throws SQLException,IllegalArgumentException{
		
		logger.info("Select any fields from table; query: " + query+" params: "+params);
		List<Map<String, Object>> hm = new ArrayList<Map<String, Object>> ();
		if (query.toUpperCase().trim().startsWith ("SELECT")){
			Connection tConnect = null;
			boolean successCon = false;
			if (constConnected){
				if (this.permCon != null){
					tConnect = permCon;
					successCon = true;
				}
			}else tConnect= createConnection();
			PreparedStatement pstmt = tConnect.prepareStatement(query);
			this.prepareParamStatement(tConnect, pstmt, params);
//			boolean fail = false;
			ResultSet rs = null;
			try {
				if (pstmt.execute ()){
					rs = pstmt.getResultSet();
					
					hm = parser.parseResultSet(rs);
					//	this.closeConnect();
					if (rs != null)rs.close();
				}
			}
			catch (Exception e){
				logger.error("error:{}", e);
			}
			finally {
				try{
					try {
						if (rs != null) rs.close();
					} catch (Exception e) {}
					try {
						if (pstmt != null)pstmt.close();
					} catch (Exception e) {}
					if (!successCon)closeConnection(tConnect);
				}catch(Exception e){}
//				if (fail) throw new IllegalArgumentException(ret.getMessage());
			}
		}		
		lastSelectResult = hm;
		return hm;
	}

	public <T>List<?> getList (Class<T> c, Map<String, Object> parameters) throws SQLException,IllegalArgumentException{
		return this.getList(c, parameters, new ArrayList<String>());
	}
	
	public <T>List<?> getList (Class<T> c, Map<String, Object> parameters, List<String> exceptFields) throws SQLException,IllegalArgumentException{
		AppEntitiesConteiner appEc = AppSimpleUtil.getAppEntitiesConteiner();
		EntityContainer eCont = appEc.getEntity(c);
		if (eCont == null){
			if (appEc.addEntity(c)){
				eCont = appEc.getEntity(c);
			}
		}
		boolean isEntity = eCont.getIsEntity();
		String tableName = eCont.getTableName();
		String schema = eCont.getSchema();
		
		if (!isEntity)throw new IllegalArgumentException("is not Entity cass");
		List<Object> hm = new ArrayList <Object> ();
		logger.info("Select any fields from table:"+ tableName+"; schema:"+schema);
//		String query = "select * from "+schema+(schema.isEmpty()?"":".")+tableName;
		IQueryBuilder qb = QueryBuilder.createBuilder(c).setExceptFields(exceptFields).select();
		String query = qb.build();
		List<Object> params = new ArrayList <Object>();
		int i = 0;
		for (Map.Entry<String, Object> elem:parameters.entrySet()){
			if (i != 0){
				query += " AND ";
			}else {
				query += " WHERE ";
			}
			query += " "+elem.getKey() + "=? ";
			params.add(elem.getValue());
			++ i;
		}
		Connection tConnect = null;
		boolean successCon = false;
		if (constConnected){
			if (this.permCon != null){
				tConnect = permCon;
				successCon = true;
			}
		}else tConnect= createConnection();
		PreparedStatement pstmt = tConnect.prepareStatement(query);
		this.prepareParamStatement(tConnect, pstmt, params);
		ResultSet rs = null;
		try {
			if (pstmt.execute ()){
				rs = pstmt.getResultSet();
				hm = parser.parseResultSetRef(c, eCont.getFields(), rs);
				if (rs != null)rs.close();
			}
		}
		catch (Exception e){
			logger.error("get list: {}", e);
//			e.printStackTrace();
		} finally {
			try{
				try {
					if (rs != null) rs.close();
				} catch (Exception e) {}
				try {
					if (pstmt != null)pstmt.close();
				} catch (Exception e) {}
				if (!successCon)closeConnection(tConnect);
			}catch(Exception e){}
		}
		return hm;
	}

	public <T>List<T> getList(Class<T> c, String whereClause, List<Object> params) throws SQLException,IllegalArgumentException{
		return this.getList(c, whereClause, params, new ArrayList<String>());
	}
	
	@SuppressWarnings("unchecked")
	public <T>List<T> getList(Class<T> c, String whereClause, List<Object> params, List<String> exceptFields) throws SQLException,IllegalArgumentException{
		AppEntitiesConteiner appEc = AppSimpleUtil.getAppEntitiesConteiner();
		EntityContainer eCont = appEc.getEntity(c);
		if (eCont == null){
			if (appEc.addEntity(c)){
				eCont = appEc.getEntity(c);
			}
		}
		boolean isEntity = eCont.getIsEntity();
		String tableName = eCont.getTableName();
		String schema = eCont.getSchema();
		if (!isEntity)throw new IllegalArgumentException("is not Entity cass");
		List<T> hm = new ArrayList <T> ();
		logger.info("Select any fields from table:"+ tableName+"; schema:"+schema);
//		String query = "select * from "+schema+(schema.isEmpty()?"":".")+tableName;
//		query += " "+whereClause;
		IQueryBuilder qb = QueryBuilder.createBuilder(c).setExceptFields(exceptFields).select();
		String query = qb.build()+" "+whereClause;
		Connection tConnect = null;
		boolean successCon = false;
		if (constConnected){
			if (this.permCon != null){
				tConnect = permCon;
				successCon = true;
			}
		}else tConnect= createConnection();
		logger.debug("query string:{}", query);
		PreparedStatement pstmt = tConnect.prepareStatement(query);
		this.prepareParamStatement(tConnect, pstmt, params);
		ResultSet rs = null;
		try {
			if (pstmt.execute ()){
				rs = pstmt.getResultSet();
				hm = (List<T>) parser.parseResultSetRef(c, eCont.getFields(), rs);
				if (rs != null)rs.close();
			}
		}
		catch (Exception e){
			logger.error("error in getList: {}", e);
		}
		finally {
			try{
				try {
					if (rs != null) rs.close();
				} catch (Exception e) {}
				try {
					if (pstmt != null)pstmt.close();
				} catch (Exception e) {}
				if (!successCon)closeConnection(tConnect);
			}catch(Exception e){}
		}
		return hm;
	}

	public <T>List<T> getListJoin(Class<T> c, String whereClause, List<Object> params, List<Class<?>> joins) throws SQLException,IllegalArgumentException{
		return this.getListJoin(c, whereClause, params, joins, new ArrayList<String>());
	}

	public <T>List<T> runBuilder (IQueryBuilder qb, List<Object> params) throws SQLException{
		String query = qb.build();
		Connection tConnect = null;
		boolean successCon = false;
		List<T> hm = new ArrayList <T> ();
		if (constConnected){
			if (this.permCon != null){
				tConnect = permCon;
				successCon = true;
			}
		}else tConnect= createConnection();
		logger.debug("query string:{}", query);
		PreparedStatement pstmt = tConnect.prepareStatement(query);
		this.prepareParamStatement(tConnect, pstmt, params);
		ResultSet rs = null;
		try {
			if (pstmt.execute ()){
				rs = pstmt.getResultSet();
//				hm = (List<T>) parseResultSetRef(c, eCont.getFields(), rs);
				EntityContainer eCont = qb.getECont();
				List<Class<?>> joins = qb.getJoinBuilders().stream().map(x->x.qBuilder.getOwnClass()).collect(Collectors.toList());
				hm = (List<T>) parser.parseResultSetRefJoin(qb.getOwnClass(), eCont.getFields(), eCont.getJoinFieldList(), eCont.getJoinFields(), rs, eCont.getIdentFieldName(), joins);
				if (rs != null)rs.close();
			}
		}
		catch (Exception e){
			logger.error("error in getList: {}", e);
		}
		finally {
			try{
				try {
					if (rs != null) rs.close();
				} catch (Exception e) {}
				try {
					if (pstmt != null)pstmt.close();
				} catch (Exception e) {}
				if (!successCon)closeConnection(tConnect);
			}catch(Exception e){}
		}
		return hm;
	}
	
	@SuppressWarnings("unchecked")
	public <T>List<T> getListJoin(Class<T> c, String whereClause, List<Object> params, List<Class<?>> joins, List<String> exceptFields) throws SQLException,IllegalArgumentException{
		AppEntitiesConteiner appEc = AppSimpleUtil.getAppEntitiesConteiner();
		EntityContainer eCont = appEc.getEntity(c);
		if (eCont == null){
			if (appEc.addEntity(c)){
				eCont = appEc.getEntity(c);
			}
		}
		boolean isEntity = eCont.getIsEntity();
		String tableName = eCont.getTableName();
		String schema = eCont.getSchema();
		if (!isEntity)throw new IllegalArgumentException("is not Entity cass");
		List<T> hm = new ArrayList <T> ();
		logger.info("Select any fields from table:"+ tableName+"; schema:"+schema);
		String identFieldName = eCont.getIdentFieldName();
		IQueryBuilder qb = QueryBuilder.createBuilder(c).joinMode(1).joinRestrictionClasses(joins).setExceptFields(exceptFields).select();
		boolean isIdent = eCont.getHasIdent ();
		
		if (!isIdent) throw new IllegalArgumentException("not found Identity field");
		String query = qb.build()+" "+whereClause;
		Connection tConnect = null;
		boolean successCon = false;
		if (constConnected){
			if (this.permCon != null){
				tConnect = permCon;
				successCon = true;
			}
		}else tConnect= createConnection();
		logger.debug("query string:{}", query);
		PreparedStatement pstmt = tConnect.prepareStatement(query);
		this.prepareParamStatement(tConnect, pstmt, params);
		ResultSet rs = null;
		try {
			if (pstmt.execute ()){
				rs = pstmt.getResultSet();
//				hm = (List<T>) parseResultSetRef(c, eCont.getFields(), rs);
				hm = (List<T>) parser.parseResultSetRefJoin(c, eCont.getFields(), eCont.getJoinFieldList(), eCont.getJoinFields(), rs, identFieldName, joins);				
				if (rs != null)rs.close();
			}
		}
		catch (Exception e){
			logger.error("error in getList: {}", e);
		}
		finally {
			try{
				try {
					if (rs != null) rs.close();
				} catch (Exception e) {}
				try {
					if (pstmt != null)pstmt.close();
				} catch (Exception e) {}
				if (!successCon)closeConnection(tConnect);
			}catch(Exception e){}
		}
		return hm;
	}
	/**
	 * get count of table rows
	 * @param c
	 * @return
	 */
	public <T>Long count (Class<T> c)throws SQLException,IllegalArgumentException{
		Long countId = 0L;
		AppEntitiesConteiner appEc = AppSimpleUtil.getAppEntitiesConteiner();
		EntityContainer eCont = appEc.getEntity(c);
		if (eCont == null){
			if (appEc.addEntity(c)){
				eCont = appEc.getEntity(c);
			}
		}
		boolean isEntity = eCont.getIsEntity();
		
		if (!isEntity)throw new IllegalArgumentException("is not Entity class");
		
		IQueryBuilder qb = QueryBuilder.createBuilder(c).select().count();
		
		Connection tConnect = null;
		boolean successCon = false;
		if (constConnected){
			if (this.permCon != null){
				tConnect = permCon;
				successCon = true;
			}
		}else tConnect= createConnection();
		PreparedStatement pstmt = tConnect.prepareStatement(qb.build());
		this.prepareParamStatement(tConnect, pstmt, Arrays.asList ());
		ResultSet rs = null;
		try {
			if (pstmt.execute ()){
				rs = pstmt.getResultSet();
				List<Object> list = parser.parseResultSetRef(RowCounter.class, appEc.getEntity(RowCounter.class).getFields(), rs);
				if (list.size() > 1) throw new IllegalArgumentException("more than 1 row for this Identity");
				if (list.isEmpty()) throw new IllegalArgumentException("Object not found");
				countId = ((RowCounter)list.get(0)).get__ROWS_COUNT();
				if (rs != null)rs.close();
			}
		}catch (IllegalArgumentException e1){
			logger.error("error:{}", e1.getMessage());
		}catch (Exception e){
			logger.error("error:{}", e);
		}finally {
			try{
				try {
					if (rs != null) rs.close();
				} catch (Exception e) {}
				try {
					if (pstmt != null)pstmt.close();
				} catch (Exception e) {}
				if (!successCon)closeConnection(tConnect);
			}catch(Exception e){}
		}
		return countId;
	}
	
	public <T>Long count (Class<T> c, String whereClause, List<Object> param)throws SQLException,IllegalArgumentException{
		Long countId = -1L;
		AppEntitiesConteiner appEc = AppSimpleUtil.getAppEntitiesConteiner();
		EntityContainer eCont = appEc.getEntity(c);
		if (eCont == null){
			if (appEc.addEntity(c)){
				eCont = appEc.getEntity(c);
			}
		}
		boolean isEntity = eCont.getIsEntity();
		String tableName = eCont.getTableName();
		String schema = eCont.getSchema();

		if (!isEntity)throw new IllegalArgumentException("is not Entity class");
		
		logger.info("Select any fields from table:"+ tableName+"; schema:"+schema);
		StringBuffer query = new StringBuffer();
		query.append("select COUNT(1) AS __ROWS_COUNT ");
		query.append(" from ");
		query.append(schema);
		query.append((schema.isEmpty()?"":"."));
		query.append(tableName);
		query.append(" "+tableName);
		boolean isIdent = eCont.getHasIdent ();
		
		if (!isIdent) throw new IllegalArgumentException("not found Identity field");
		
		query.append (" "+whereClause);
		logger.info("query: "+query+";params:"+param);
		Connection tConnect = null;
		boolean successCon = false;
		if (constConnected){
			if (this.permCon != null){
				tConnect = permCon;
				successCon = true;
			}
		}else tConnect= createConnection();
		PreparedStatement pstmt = tConnect.prepareStatement(query.toString());
		this.prepareParamStatement(tConnect, pstmt, param);
		ResultSet rs = null;
		try {
			if (pstmt.execute ()){
				rs = pstmt.getResultSet();
				List<Object> list = parser.parseResultSetRef(RowCounter.class, appEc.getEntity(RowCounter.class).getFields(), rs);
				if (list.size() > 1) throw new IllegalArgumentException("more than 1 row for this Identity");
				if (list.isEmpty()) throw new IllegalArgumentException("Object not found");
				countId = ((RowCounter)list.get(0)).get__ROWS_COUNT();
				if (rs != null)rs.close();
			}
		}catch (IllegalArgumentException e1){
			logger.error("error:{}", e1.getMessage());
		}catch (Exception e){
			logger.error("error:{}", e);
		}finally {
			try{
				try {
					if (rs != null) rs.close();
				} catch (Exception e) {}
				try {
					if (pstmt != null)pstmt.close();
				} catch (Exception e) {}
				if (!successCon)closeConnection(tConnect);
			}catch(Exception e){}
		}
		return countId;
	}

	public <T>T getObject (Class<T> c, Object id) throws SQLException,IllegalArgumentException{
		return this.getObject(c, id, new ArrayList<String>());
	}
	
	@SuppressWarnings("unchecked")
	public <T>T getObject (Class<T> c, Object id, List<String> exceptFields) throws SQLException,IllegalArgumentException{
		AppEntitiesConteiner appEc = AppSimpleUtil.getAppEntitiesConteiner();
		EntityContainer eCont = appEc.getEntity(c);
		if (eCont == null){
			if (appEc.addEntity(c)){
				eCont = appEc.getEntity(c);
			}
		}
		boolean isEntity = eCont.getIsEntity();
		T hm = null;
		try {
			hm = c.newInstance();
		} catch (InstantiationException | IllegalAccessException e1) {
			logger.error("error:{}", e1);
		}
		
		if (!isEntity)throw new IllegalArgumentException("is not Entity class");
		
		IQueryBuilder qb = QueryBuilder.createBuilder(c).setExceptFields(exceptFields).select();
		
		Connection tConnect = null;
		boolean successCon = false;
		if (constConnected){
			if (this.permCon != null){
				tConnect = permCon;
				successCon = true;
			}
		}else tConnect= createConnection();
		String query = qb.build()+" where "+eCont.getIdentFieldName()+"=?";
		logger.debug("query string:{}", query);
		PreparedStatement pstmt = tConnect.prepareStatement(query);
		this.prepareParamStatement(tConnect, pstmt, Arrays.asList (id));
		ResultSet rs = null;
		try {
			if (pstmt.execute ()){
				rs = pstmt.getResultSet();
				List<Object> list = parser.parseResultSetRef(c, eCont.getFields(), rs);
				if (list.size() > 1) throw new IllegalArgumentException("more than 1 row for this Identity");
				if (list.isEmpty()) throw new IllegalArgumentException("Object not found");
				hm = (T) list.get(0);
				if (rs != null)rs.close();
			}
		}catch (IllegalArgumentException e1){
			logger.error("error:{}", e1.getMessage());
		}catch (Exception e){
			logger.error("error:{}", e);
		}finally {
			try{
				try {
					if (rs != null) rs.close();
				} catch (Exception e) {}
				try {
					if (pstmt != null)pstmt.close();
				} catch (Exception e) {}
				if (!successCon)closeConnection(tConnect);
			}catch(Exception e){}
		}
		return hm;
	}

	public <T>T getObject (Class<T> c, String whereClause, List<Object> param) throws SQLException,IllegalArgumentException{
		return this.getObject(c, whereClause, param, new ArrayList<String>());
	}
	
	@SuppressWarnings("unchecked")
	public <T>T getObject (Class<T> c, String whereClause, List<Object> param, List<String> exceptFields) throws SQLException,IllegalArgumentException{
		AppEntitiesConteiner appEc = AppSimpleUtil.getAppEntitiesConteiner();
		EntityContainer eCont = appEc.getEntity(c);
		if (eCont == null){
			if (appEc.addEntity(c)){
				eCont = appEc.getEntity(c);
			}
		}
		boolean isEntity = eCont.getIsEntity();
		T hm = null;
		try {
			hm = c.newInstance();
		} catch (InstantiationException | IllegalAccessException e1) {
			logger.error("error:{}", e1);
		}

		if (!isEntity)throw new IllegalArgumentException("is not Entity class");
		
		IQueryBuilder qb = QueryBuilder.createBuilder(c).setExceptFields(exceptFields).select();
		boolean isIdent = eCont.getHasIdent ();
		
		if (!isIdent) throw new IllegalArgumentException("not found Identity field");
		String query = qb.build()+" "+whereClause;
		Connection tConnect = null;
		boolean successCon = false;
		if (constConnected){
			if (this.permCon != null){
				tConnect = permCon;
				successCon = true;
			}
		}else tConnect= createConnection();
		logger.debug("query string:{}", query);
		PreparedStatement pstmt = tConnect.prepareStatement(query);
		this.prepareParamStatement(tConnect, pstmt, param);
		ResultSet rs = null;
		try {
			if (pstmt.execute ()){
				rs = pstmt.getResultSet();
				List<Object> list = parser.parseResultSetRef(c, eCont.getFields(), rs);
				if (list.size() > 1) throw new IllegalArgumentException("more than 1 row for this Identity");
				if (list.isEmpty()) throw new IllegalArgumentException("Object not found by clause:"+whereClause);
				hm = (T) list.get(0);
				if (rs != null)rs.close();
			}
		}catch (IllegalArgumentException e1){
			logger.error("error:{}", e1.getMessage());
		}catch (Exception e){
			logger.error("error:{}", e);
		}finally {
			try{
				try {
					if (rs != null) rs.close();
				} catch (Exception e) {}
				try {
					if (pstmt != null)pstmt.close();
				} catch (Exception e) {}
				if (!successCon)closeConnection(tConnect);
			}catch(Exception e){}
		}
		return hm;
	}

	public <T>T getObjectJoin (Class<T> c, String whereClause, List<Object> param) throws SQLException,IllegalArgumentException{
		return this.getObjectJoin(c, whereClause, param, new ArrayList<String>());
	}
	
	public <T>T getObjectJoin (Class<T> c, String whereClause, List<Object> param, List<String> exceptFields) throws SQLException,IllegalArgumentException{
		return getObjectJoin(c, whereClause, param, exceptFields, Arrays.asList());
	}
	
	@SuppressWarnings("unchecked")
	public <T>T getObjectJoin (Class<T> c, String whereClause, List<Object> param, List<String> exceptFields,
			List<Class<?>> rejectedJoins) throws SQLException,IllegalArgumentException{
		AppEntitiesConteiner appEc = AppSimpleUtil.getAppEntitiesConteiner();
		EntityContainer eCont = appEc.getEntity(c);
		if (eCont == null){
			if (appEc.addEntity(c)){
				eCont = appEc.getEntity(c);
			}
		}
		boolean isEntity = eCont.getIsEntity();
		T hm = null;
		try {
			hm = c.newInstance();
		} catch (InstantiationException | IllegalAccessException e1) {
			logger.error("error:{}", e1);
		}

		if (!isEntity)throw new IllegalArgumentException("is not Entity class");
		String identFieldName = eCont.getIdentFieldName();
		IQueryBuilder qb = QueryBuilder.createBuilder(c).joinMode(1).select();
		if (!rejectedJoins.isEmpty())qb.setRejectedJoinClasses(rejectedJoins);
		boolean isIdent = eCont.getHasIdent ();
		
		if (!isIdent) throw new IllegalArgumentException("not found Identity field");
		String query = qb.build()+" "+whereClause;
		Connection tConnect = null;
		boolean successCon = false;
		if (constConnected){
			if (this.permCon != null){
				tConnect = permCon;
				successCon = true;
			}
		}else tConnect= createConnection();
		logger.debug("query string:{}", query);
		PreparedStatement pstmt = tConnect.prepareStatement(query);
		this.prepareParamStatement(tConnect, pstmt, param);
		ResultSet rs = null;
		try {
			if (pstmt.execute ()){
				rs = pstmt.getResultSet();
				List<Object> list = parser.parseResultSetRefJoin(c, eCont.getFields(), eCont.getJoinFieldList(), eCont.getJoinFields(), rs, identFieldName);
				if (list.size() > 1) throw new IllegalArgumentException("more than 1 row for this Identity");
				if (list.isEmpty()) throw new IllegalArgumentException("Object not found by clause:"+whereClause);
				hm = (T) list.get(0);
				if (rs != null)rs.close();
			}
		}catch (IllegalArgumentException e1){
			logger.error("error:{}", e1.getMessage());
		}catch (Exception e){
			logger.error("error:{}", e);
		}finally {
			try{
				try {
					if (rs != null) rs.close();
				} catch (Exception e) {}
				try {
					if (pstmt != null)pstmt.close();
				} catch (Exception e) {}
				if (!successCon)closeConnection(tConnect);
			}catch(Exception e){}
		}
		return hm;
	}

	public <T>T getObjectJoin (Class<T> c, Object id) throws SQLException,IllegalArgumentException{
		return this.getObjectJoin(c, id, null);
	}
	
	@SuppressWarnings("unchecked")
	public <T>T getObjectJoin (Class<T> c, Object id, List<Class<?>> joins) throws SQLException,IllegalArgumentException{
		AppEntitiesConteiner appEc = AppSimpleUtil.getAppEntitiesConteiner();
		EntityContainer eCont = appEc.getEntity(c);
		if (eCont == null){
			if (appEc.addEntity(c)){
				eCont = appEc.getEntity(c);
			}
		}
		boolean isEntity = eCont.getIsEntity();
		T hm = null;
		try {
			hm = c.newInstance();
		} catch (InstantiationException | IllegalAccessException e1) {
			logger.error("error:{}", e1);
		}

		if (!isEntity)throw new IllegalArgumentException("is not Entity class");
		String identFieldName = eCont.getIdentFieldName();
		IQueryBuilder qb = QueryBuilder.createBuilder(c).joinMode(1).select();
		boolean isIdent = eCont.getHasIdent ();
		
		if (!isIdent) throw new IllegalArgumentException("not found Identity field");
		qb.joinRestrictionClasses(joins).where(Where.where(clause.w().eq(qb.getOwnClassName()+"."+identFieldName, id)));
		String query = qb.build();
		Connection tConnect = null;
		boolean successCon = false;
		if (constConnected){
			if (this.permCon != null){
				tConnect = permCon;
				successCon = true;
			}
		}else tConnect= createConnection();
		logger.debug("query string:{}", query);
		PreparedStatement pstmt = tConnect.prepareStatement(query);
		this.prepareParamStatement(tConnect, pstmt, Arrays.asList(id));
		ResultSet rs = null;
		try {
			if (pstmt.execute ()){
				rs = pstmt.getResultSet();
				List<Object> list = parser.parseResultSetRefJoin(c, eCont.getFields(), eCont.getJoinFieldList(), eCont.getJoinFields(), rs, identFieldName);
				if (list.size() > 1) throw new IllegalArgumentException("more than 1 row for this Identity");
				if (list.isEmpty()) throw new IllegalArgumentException("Object not found by clause:");
				hm = (T) list.get(0);
				if (rs != null)rs.close();
			}
		}catch (IllegalArgumentException e1){
			logger.error("error:{}", e1.getMessage());
		}catch (Exception e){
			logger.error("error:{}", e);
		}finally {
			try{
				try {
					if (rs != null) rs.close();
				} catch (Exception e) {}
				try {
					if (pstmt != null)pstmt.close();
				} catch (Exception e) {}
				if (!successCon)closeConnection(tConnect);
			}catch(Exception e){}
		}
		return hm;
	}

	public <T>T insert (T o) throws IllegalArgumentException, IllegalAccessException, SQLException{
		return this.insert(o, new ArrayList<String>());
	}
	
	public <T>T insert (T o, List<String> exceptFields) throws IllegalArgumentException, IllegalAccessException, SQLException{
		Long t = System.currentTimeMillis();
		int resultCount = 0;
		T resultObj = null;
		@SuppressWarnings("unchecked")
		Class<T> c = (Class<T>) o.getClass();
		AppEntitiesConteiner appEc = AppSimpleUtil.getAppEntitiesConteiner();
		EntityContainer eCont = appEc.getEntity(c);
		if (eCont == null){
			if (appEc.addEntity(c)){
				eCont = appEc.getEntity(c);
			}
		}
		boolean isEntity = eCont.getIsEntity();
		String primaryKey = eCont.getIdentFieldName();
		if (!isEntity)throw new IllegalArgumentException("is not Entity class");
		List<Object> params = new ArrayList<Object>();
		List<java.lang.reflect.Field> fields= eCont.getFields();
		
		boolean hasExcepts = !exceptFields.isEmpty();
		for (java.lang.reflect.Field field:fields){
			if (hasExcepts){
				if (exceptFields.stream().filter(x->x.equalsIgnoreCase(field.getName())).findFirst().isPresent())continue;
			}
			if (field.getName().equalsIgnoreCase(primaryKey) && eCont.isIdentIsAutoGenerated()) continue;
			field.setAccessible(true);
			params.add(field.get(o));
		}
		/*
		 * create Query String
		 */
		IQueryBuilder qb = QueryBuilder.createBuilder(c).setExceptFields(exceptFields).insert();
		Object pkVal = null;
		if (constConnected){
			Long t2 = System.currentTimeMillis();
			if (this.useTransaction){
				resultCount = this.addToTransaction(qb.build(), params);//, primaryKey);
				if (resultCount>0) pkVal = this.getGKeys().get(0);
			}else{
				if (this.startTransaction()){
					resultCount = this.addToTransaction(qb.build(), params);//, primaryKey);
					if (resultCount>0) pkVal = this.getGKeys().get(0);
					this.endTransaction();
				}
			}
			logger.debug("only query time:{}", (System.currentTimeMillis()-t2));
		}else{
			ResultSet rs = null;
			PreparedStatement pstmt = null;
			Connection tConnect = null;
			try{
				tConnect = createConnection();
				pstmt = tConnect.prepareStatement(qb.build(), new String[]{primaryKey});
				this.prepareParamStatement(tConnect, pstmt, params);
				resultCount = pstmt.executeUpdate();
				tConnect.commit();
				
	//			boolean fail = false;
				rs = pstmt.getGeneratedKeys();
				gkeys.clear();
				gkeys.addAll(parser.parseGeneratedKeys(rs));
				if (resultCount>0) pkVal = gkeys.get(0);
			}catch (Exception e){
				logger.error("error in insert {}", e);
			}
			finally {
				try{
					try {
						if (rs != null) rs.close();
					} catch (Exception e) {}
					try {
						if (pstmt != null)pstmt.close();
					} catch (Exception e) {}
					closeConnection(tConnect);
				}catch(Exception e){}
			}
		}
		if (resultCount>0){
			resultObj = o;
			eCont.getIdentField().setAccessible(true);
			eCont.getIdentField().set(resultObj, pkVal);
		}
		logger.debug("full insert time:{}", (System.currentTimeMillis()-t));
		return resultObj;
	}
	
	public int update (Object o,  Map<String, Object> parameters) throws IllegalArgumentException, IllegalAccessException, SQLException{
		return this.update(o, parameters, new ArrayList<String>());
	}
	
	public int update (Object o,  Map<String, Object> parameters, List<String> exceptFields) throws IllegalArgumentException, IllegalAccessException, SQLException{
		int resultCount = 0;
		Class<?> c = o.getClass();
		AppEntitiesConteiner appEc = AppSimpleUtil.getAppEntitiesConteiner();
		EntityContainer eCont = appEc.getEntity(c);
		if (eCont == null){
			if (appEc.addEntity(c)){
				eCont = appEc.getEntity(c);
			}
		}
		boolean isEntity = eCont.getIsEntity();
		String primaryKey = eCont.getIdentFieldName();
		if (!isEntity)throw new IllegalArgumentException("is not Entity class");
		List<Object> params = new ArrayList<Object>();
		List<java.lang.reflect.Field> fields= eCont.getFields();
		boolean hasExcepts = !exceptFields.isEmpty();
		for (java.lang.reflect.Field field:fields){
			if (hasExcepts){
				if (exceptFields.stream().filter(x->x.equalsIgnoreCase(field.getName())).findFirst().isPresent())continue;
			}
			if (field.getName().equalsIgnoreCase(eCont.getIdentFieldName()) && eCont.isIdentIsAutoGenerated()) continue;
			field.setAccessible(true);
			params.add(field.get(o));
		}
		String where = " ";
		int i = 0;
		for (Map.Entry<String, Object> elem:parameters.entrySet()){
			if (i != 0){
				where += " AND ";
			}else {
				where += " WHERE ";
			}
			where += " "+elem.getKey() + "=? ";
			params.add(elem.getValue());
			++ i;
		}
		/*
		 * create Query String
		 */
		IQueryBuilder qb = QueryBuilder.createBuilder(c).setExceptFields(exceptFields).update();
		
		if (constConnected){
			if (this.useTransaction){
				resultCount = this.addToTransaction(qb.build()+where, params);
			}else{
				if (this.startTransaction()){
					resultCount = this.addToTransaction(qb.build()+where, params);
					this.endTransaction();
				}
			}
		}else{
			ResultSet rs = null;
			PreparedStatement pstmt = null;
			Connection tConnect = null;
			try{
				tConnect = createConnection();
				pstmt = tConnect.prepareStatement(qb.build(), new String[]{primaryKey});
				this.prepareParamStatement(tConnect, pstmt, params);
				resultCount = pstmt.executeUpdate();
				tConnect.commit();
				
	//			boolean fail = false;
				rs = pstmt.getGeneratedKeys();
				gkeys.clear();
				gkeys.addAll(parser.parseGeneratedKeys(rs));
			}catch (Exception e){
				logger.error("error in insert {}", e);
			}
			finally {
				try{
					try {
						if (rs != null) rs.close();
					} catch (Exception e) {}
					try {
						if (pstmt != null)pstmt.close();
					} catch (Exception e) {}
					closeConnection(tConnect);
				}catch(Exception e){}
			}
		}
		return resultCount;
	}
	
	public Object update (Object o) throws IllegalArgumentException, IllegalAccessException, SQLException{
		int resultCount = 0;
		Object resultObj = null;
		Class<?> c = o.getClass();
		AppEntitiesConteiner appEc = AppSimpleUtil.getAppEntitiesConteiner();
		EntityContainer eCont = appEc.getEntity(c);
		if (eCont == null){
			if (appEc.addEntity(c)){
				eCont = appEc.getEntity(c);
			}
		}
		boolean isEntity = eCont.getIsEntity();
		String primaryKey = eCont.getIdentFieldName();
		if (!isEntity)throw new IllegalArgumentException("is not Entity class");
		List<Object> params = new ArrayList<Object>();
		List<java.lang.reflect.Field> fields= eCont.getFields();
		Object idVal = null;
		for (java.lang.reflect.Field field:fields){
			field.setAccessible(true);
			if (field.getName().equalsIgnoreCase(eCont.getIdentFieldName())){
				idVal = field.get(o);
				if (eCont.isIdentIsAutoGenerated())continue;
			}
			params.add(field.get(o));
		}
		String where = eCont.getIdentFieldName()+"=?";
		where = where.isEmpty()?"":" where "+where;
		params.add(idVal);
		/*
		 * create Query String
		 */
		IQueryBuilder qb = QueryBuilder.createBuilder(c).update();
		Object pkVal = null;
		if (constConnected){
			if (this.useTransaction){
				resultCount = this.addToTransaction(qb.build()+where, params);//, primaryKey);
				if (resultCount>0) pkVal = this.getGKeys().get(0);
			}else{
				if (this.startTransaction()){
					resultCount = this.addToTransaction(qb.build()+where, params);//, primaryKey);
					if (resultCount>0) pkVal = this.getGKeys().get(0);
					this.endTransaction();
				}
			}
		}else{
			ResultSet rs = null;
			PreparedStatement pstmt = null;
			Connection tConnect = null;
			try{
				tConnect = createConnection();
				pstmt = tConnect.prepareStatement(qb.build(), new String[]{primaryKey});
				this.prepareParamStatement(tConnect, pstmt, params);
				resultCount = pstmt.executeUpdate();
				tConnect.commit();
				
	//			boolean fail = false;
				rs = pstmt.getGeneratedKeys();
				gkeys.clear();
				gkeys.addAll(parser.parseGeneratedKeys(rs));
			}catch (Exception e){
				logger.error("error in insert {}", e);
			}
			finally {
				try{
					try {
						if (rs != null) rs.close();
					} catch (Exception e) {}
					try {
						if (pstmt != null)pstmt.close();
					} catch (Exception e) {}
					closeConnection(tConnect);
				}catch(Exception e){}
			}
		}
		if (resultCount>0){
			resultObj = o;
			eCont.getIdentField().setAccessible(true);
			eCont.getIdentField().set(resultObj, pkVal);
		}
		return resultObj;
	}

	public String getJndi_name() {
		return jndi_name;
	}

	public void setJndi_name(String jndi_name) {
		this.jndi_name = jndi_name;
	}
	
	public ResultSetParser getParser (){
		return this.parser;
	}
	
	@Override
	protected void finalize() throws Throwable{  
		this.closePermConnection();
	    super.finalize();  
	}  
}

