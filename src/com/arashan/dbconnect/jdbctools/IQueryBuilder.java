package com.arashan.dbconnect.jdbctools;

import java.util.List;

/**
 * 
 * @author luger
 *
 */
public interface IQueryBuilder {
	/**
	 * 
	 * @return SQL string
	 */
	public String getResultQuery ();
	/**
	 * setting select mode
	 * @return this (DSL)
	 */
	public IQueryBuilder select ();
	/**
	 * setting update mode
	 * @return this
	 */
	public IQueryBuilder joinMode (int joinMode);
	
	public IQueryBuilder joinRestrictionClasses (List<Class<?>> joinsRestrictions);
	public IQueryBuilder setAcceptedJoinClasses (List<Class<?>> joinsRestrictions);
	public IQueryBuilder setAcceptedJoinClasses (Class<?>... joinsRestrictions);
	public IQueryBuilder setRejectedJoinClasses (List<Class<?>> joinsRestrictions);
	public IQueryBuilder setRejectedJoinClasses (Class<?>... joinsRestrictions);
	/**
	 * setting update mode
	 * @return this
	 */
	public IQueryBuilder update ();
	/**
	 * setting insert mode
	 * @return this
	 */
	public IQueryBuilder insert ();
	/**
	 * setting delete mode
	 * @return this
	 */
	public IQueryBuilder delete ();
	/**
	 * joining any table with name
	 * @param tableName - name of Table in DB
	 * @return this
	 * @throws QueryMethodException
	 */
	public IQueryBuilder join (String tableName) throws QueryMethodException;
	/**
	 * joining any table formed with Entity Container
	 * @param joinCont - Entity Object
	 * @param pk - primary key
	 * @param fk - foreign key
	 * @return this
	 * @throws QueryMethodException
	 */
	public IQueryBuilder join (EntityContainer joinCont, String pk, String fk) throws QueryMethodException;
	/**
	 * joining any table formed with Class with auto detecting pk and fk
	 * @param c - Class
	 * @return this
	 * @throws QueryMethodException
	 */
	public IQueryBuilder join (Class<?> c) throws QueryMethodException;
	/**
	 * joining any table formed with Class without auto detecting pk and fk
	 * @param c
	 * @param pk - primary key
	 * @param fk - foreign key
	 * @return this
	 * @throws QueryMethodException
	 */
	public IQueryBuilder join (Class<?> c, String pk, String fk) throws QueryMethodException;
	/**
	 * joining any table formed with QueryBuilder
	 * @param qb - QueryBuilder
	 * @param pk - primary key
	 * @param fk - foreign key
	 * @return this
	 * @throws QueryMethodException
	 */
	public IQueryBuilder join (IQueryBuilder qb, String pk, String fk) throws QueryMethodException;
	/**
	 * union to SQL 
	 * @param qb
	 * @return this
	 */
	public IQueryBuilder union (IQueryBuilder qb);
	/**
	 * unionAll to SQL 
	 * @param qb
	 * @return this
	 */
	public IQueryBuilder unionAll (IQueryBuilder qb);
	/**
	 * get count of selected rows 
	 * @return this
	 */
	public IQueryBuilder count ();
	/**
	 * building SQL script
	 * @return SQL script
	 */
	public String build ();
	/**
	 * setting fields names for excetion them from query
	 */
	public IQueryBuilder setExceptFields (List<String> exceptFields);
	public int getQueryMode();
	public String getTbName();
	public String getSchema();
	public List<String> getFieldsName();
	public String getOwnClassName();
	public Class<?> getOwnClass();
	public List<On> getJoinBuilders ();
	public EntityContainer getECont ();
	/**
	 * where clauses
	 * @param clause
	 * @return
	 */
	public IQueryBuilder where (IWhere clause);
	
	/**
	 * fetch query. bad code..
	 * @param conn
	 * @return
	 */
	public <T>List<T> fetch(DBconnector conn);
}
