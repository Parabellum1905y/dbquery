package com.arashan.dbconnect.jdbctools;

import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arashan.dbconnect.jdbctools.listeners.AppSimpleUtil;

public class ResultSetParser {
	private static final Logger logger = LoggerFactory.getLogger(ResultSetParser.class);
	
	public List<Object> parseGeneratedKeys(ResultSet rs)throws Exception{
		List<Object> result = new ArrayList<Object>();
		
		ResultSetMetaData rsmd = rs.getMetaData();
		
		while (rs.next()){
			Object element = null;
			int col = 1;
			int type = rsmd.getColumnType(col);
			element = getObjectByType(type, rs, col, null);
			result.add(element);
		}
		return result;
	}

	protected Object getObjectByType (int type, ResultSet rs, int col, Class<?> c) throws SQLException{
		Object element = null;
		ResultSetMetaData rsmd = rs.getMetaData();
		switch(type){
			case Types.INTEGER :
				Integer i = new Integer (rs.getInt(col));
				element = rs.wasNull()?rs.getObject(col):i;
				break;
			case Types.DOUBLE :
				Double d = new Double (rs.getDouble(col));
				element = rs.wasNull()?rs.getObject(col):d;
				break;
			case Types.BIGINT :
				Long l = new Long(rs.getLong(col));
				element = rs.wasNull()?rs.getObject(col):l;
				break;
			case Types.SMALLINT :
				Integer si = new Integer (rs.getInt(col));
				element = rs.wasNull()?rs.getObject(col):si;
				break;
			case Types.VARCHAR :
				element = rs.getString(col);
				break;
			case Types.CHAR :
				element = rs.getString(col);
				break;
			case Types.DATE :
				element = new Date(rs.getDate(col).getTime());
				break;
			case Types.TIMESTAMP :
				element = new Date (rs.getTimestamp(col).getTime());
				break;
			case Types.TIME :
				element = new Date (rs.getTime(col).getTime());
				break;
			case Types.JAVA_OBJECT:
				element = rs.getObject(col);
				break;
			case Types.BLOB:
				element = rs.getBlob(col);
				break;
			case Types.SQLXML:
				element = rs.getSQLXML(col);
				break;
			case Types.DECIMAL:
				BigDecimal bd = rs.getBigDecimal(col);
				element = rs.wasNull()?rs.getObject(col):bd;
				break;
			case Types.NUMERIC:
				BigDecimal bd2 = rs.getBigDecimal(col);
				element = rs.wasNull()?rs.getObject(col):bd2;
				break;
			case Types.NULL :	
				element = rs.getObject(col);
				break;
			case Types.ARRAY :
				java.sql.Array arr = rs.getArray(col);
				if (arr == null){
					element = null;
				}else{
					if (c == null){
						element = Arrays.copyOf((Object[])arr.getArray(), ((Object[])arr.getArray()).length, Object[].class);
					}else{
						Class<?> any = c;
						if (any.isArray()){
							Class<?> component = any.getComponentType();
							if (component.equals(String.class)){
								element = Arrays.copyOf((Object[])arr.getArray(), ((Object[])arr.getArray()).length, String[].class);
							}else if (component.equals(Date.class)){
								element = Arrays.copyOf((Object[])arr.getArray(), ((Object[])arr.getArray()).length, Date[].class);
							}else if (component.equals(Long.class)){
								element = Arrays.copyOf((Object[])arr.getArray(), ((Object[])arr.getArray()).length, Long[].class);
							}else if (component.equals(Float.class)){
								element = Arrays.copyOf((Object[])arr.getArray(), ((Object[])arr.getArray()).length, Float[].class);
							}else if (component.equals(Integer.class)){
								element = Arrays.copyOf((Object[])arr.getArray(), ((Object[])arr.getArray()).length, Integer[].class);
							}else if (component.equals(Double.class)){
								element = Arrays.copyOf((Object[])arr.getArray(), ((Object[])arr.getArray()).length, Double[].class);
							}else if (component.equals(BigDecimal.class)){
								element = Arrays.copyOf((Object[])arr.getArray(), ((Object[])arr.getArray()).length, BigDecimal[].class);
							}
						}else{
							element = null;
						}
					}
				}
				break;
			default:{
				if (rsmd.getColumnClassName(col).equals("java.lang.String")){
					element = rs.getString(col);
				}else if (rsmd.getColumnClassName(col).equals("java.lang.Long")){
					l = new Long(rs.getLong(col));
					element = rs.wasNull()?rs.getObject(col):l;
				}else if (rsmd.getColumnClassName(col).equals("java.lang.Date")){
					element = new Date (rs.getDate(col).getTime());
				}else if (rsmd.getColumnClassName(col).equals("java.sql.Date")){
					element = new Date (rs.getDate(col).getTime());
				}else{
					if (rs.getObject(col) != null){
						si = new Integer (rs.getInt(col));
						element = rs.wasNull()?rs.getObject(col):si;
					}else {
						element = null;
					}
				}
			}
		}
		return element;
	}

	public List<Map<String, Object>> parseResultSet(ResultSet rs)throws Exception{
		return this.parseResultSet(rs, null);
	}

	public List<Map<String, Object>> parseResultSet(ResultSet rs, Class<?>c)throws Exception{
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		
		ResultSetMetaData rsmd = rs.getMetaData();
		
		while (rs.next()){
			HashMap<String, Object> element = new HashMap<String, Object> ();
			for(int col = 1; col <= rsmd.getColumnCount(); col++){
				int type = rsmd.getColumnType(col);
				String colName = rsmd.getColumnLabel(col).toUpperCase();
				try {
					element.put(colName, getObjectByType(type, rs, col, c));
				}catch(Exception e){
					element.put(colName, null);
				}
			}
			result.add(element);
		}
		return result;
	}

	protected <T>List<Object> parseResultSetRef(Class<T> c, List<java.lang.reflect.Field> fields, ResultSet rs)throws Exception{
		return this.parseResultSetRef(c, fields, rs, "");
	}
	
	protected <T>List<Object> parseResultSetRef(Class<T> c, List<java.lang.reflect.Field> fields, ResultSet rs, String preffix)throws Exception{
		List<Object> result = new ArrayList<Object>();
		ResultSetMetaData rsmd = rs.getMetaData();
		while (rs.next()){ 
			Object o = c.getConstructor().newInstance();
			for(int col = 1; col <= rsmd.getColumnCount(); col++){
				int type = rsmd.getColumnType(col);
				String colName = rsmd.getColumnLabel(col).toUpperCase();
				for (java.lang.reflect.Field fi:fields){
					fi.setAccessible(true);
					String fName = preffix+fi.getName();
					if (fName.equalsIgnoreCase(colName)){
						try {
							fi.set(o, getObjectByType (type, rs, col, fi.getType()));
						}catch (Exception e){logger.error(colName+" | "+e.getMessage());}
					}
				}
			}
			result.add(o);
		}
		return result;
	}

	/**
	 * parsinf result to array of objects with selection all joined tables
	 * @param c
	 * @param fields
	 * @param hm
	 * @param identFieldName
	 * @param preffix
	 * @return
	 * @throws Exception
	 */
	protected <T>Map<Object, Object> parseResultSetRefJoin(Class<T> c, List<java.lang.reflect.Field> fields, 
			List<Map<String, Object>> hm, String identFieldName, String preffix)throws Exception{
		Map<Object, Object> mappedById = new HashMap<>();
		hm.forEach(X-> { 
			Object o = null;
			try {
				o = c.getConstructor().newInstance();
				Object pk = null;
				for(String key:X.keySet()){
					for (java.lang.reflect.Field fi:fields){
						fi.setAccessible(true);
						if ((preffix+fi.getName()).equalsIgnoreCase(key)){
							try {
								if ((preffix+fi.getName()).equalsIgnoreCase(key)){
									try {
										Object tmp = X.get(key);
										fi.set(o, tmp);
										if (fi.getName().equalsIgnoreCase(identFieldName)){
											pk = tmp;
										}
									}catch (Exception e){logger.error(key+" | "+e.getMessage());}
								}
							}catch (Exception e){logger.error(key+" | "+e.getMessage());}
						}
					}
				}
				if (pk != null)mappedById.put(pk, o);
			} catch (Exception e1) {
				logger.error("Invokation error:{}", e1);
			}
		});
		return mappedById;
	}
	
	/**
	 * Map parsed Map result set to map of Objects
	 * @param c- class for mapping
	 * @param hm - parsed result in Map
	 * @return
	 * @throws Exception
	 */
	public <T>Map<Object, Object> parseResultSetRef(Class<T> c, List<Map<String, Object>> hm)throws Exception{
		Map<Object, Object> mappedById = new HashMap<>();
		AppEntitiesConteiner appEc = AppSimpleUtil.getAppEntitiesConteiner();
		EntityContainer eCont = appEc.getEntity(c);
		if (eCont == null){
			if (appEc.addEntity(c)){
				eCont = appEc.getEntity(c);
			}
		}
		boolean isEntity = eCont.getIsEntity();
		String primaryKey = eCont.getIdentFieldName();
		if (!isEntity)throw new IllegalArgumentException("is not Entity class");
		List<java.lang.reflect.Field> fields= eCont.getFields();
		hm.forEach(X-> { 
			Object o = null;
			try {
				o = c.getConstructor().newInstance();
				Object pk = null;
				for(String key:X.keySet()){
					for (java.lang.reflect.Field fi:fields){
						fi.setAccessible(true);
						if (fi.getName().equalsIgnoreCase(key)){
							try {
								if (fi.getType().isArray()){
									Class<?> component = fi.getType().getComponentType();
									Object tmp[] = (Object[])X.get(key);
									if (component.equals(String.class)){
										fi.set(o, Arrays.copyOf(tmp, tmp.length, String[].class));
									}else if (component.equals(Date.class)){
										fi.set(o, Arrays.copyOf(tmp, tmp.length, Date[].class));
									}else if (component.equals(Long.class)){
										fi.set(o, Arrays.copyOf(tmp, tmp.length, Long[].class));
									}else if (component.equals(Float.class)){
										fi.set(o, Arrays.copyOf(tmp, tmp.length, Float[].class));
									}else if (component.equals(Integer.class)){
										fi.set(o, Arrays.copyOf(tmp, tmp.length, Integer[].class));
									}else if (component.equals(Double.class)){
										fi.set(o, Arrays.copyOf(tmp, tmp.length, Double[].class));
									}else if (component.equals(BigDecimal.class)){
										fi.set(o, Arrays.copyOf(tmp, tmp.length, BigDecimal[].class));
									}
								}else{
									Object tmp = X.get(key);
									fi.set(o, tmp);
									if (fi.getName().equalsIgnoreCase(primaryKey)){
										pk = tmp;
									}
								}
							}catch (Exception e){logger.error(key+" | "+e.getMessage());}
						}
					}
				}
				if (pk != null)mappedById.put(pk, o);
			} catch (Exception e1) {
				logger.error("Invokation error:{}", e1);
			}
		});
		return mappedById;
	}

	protected <T>List<Object> parseResultSetRefJoin(Class<T> c, List<java.lang.reflect.Field> fields, 
			List<java.lang.reflect.Field> joinFields,
			Map<String,Map<Class<?>,Join>> joinMap,
			ResultSet rs, String identFieldName)throws Exception{
		return this.parseResultSetRefJoin(c, fields, joinFields, joinMap, rs, identFieldName, null);
	}
	
	protected <T>List<Object> parseResultSetRefJoin(Class<T> c, List<java.lang.reflect.Field> fields, 
													List<java.lang.reflect.Field> joinFields,
													Map<String,Map<Class<?>,Join>> joinMap,
													ResultSet rs, String identFieldName,
													List<Class<?>> joins)throws Exception{
		List<Object> result = new ArrayList<Object>();
		Map<Object, Object> mappedById = new HashMap<>();
		List<Map<String, Object>> hm = parseResultSet(rs, c);
		mappedById = parseResultSetRefJoin (c,fields, hm, identFieldName, "");
		if (mappedById != null && !mappedById.isEmpty()) result = new ArrayList<>(mappedById.values());
		if (joinMap != null){
			int joinCounter = 0;
			for (java.lang.reflect.Field fi:joinFields){
				fi.setAccessible(true);
				Map<Class<?>, Join> mJoin = joinMap.get(fi.getName());
				if (mJoin!=null){
					for (Class<?> clazz:mJoin.keySet()){
						if (joins != null){
							boolean found = joins.stream().filter(X->X.getName().equalsIgnoreCase(clazz.getName())).findFirst().isPresent();
							if (!found)continue;
						}
						String joinTable = mJoin.get(clazz).getTb();
//						logger.info("joinTable:{},{},{}", joinTable,fi.getType(),clazz);
						int joinMode = mJoin.get(clazz).getJoinMode();
						Class<?> cc = fi.getType();
						if (fi.getType().getName().contains("List")){
							ParameterizedType pt = (ParameterizedType) fi.getGenericType();
							cc = (Class<?>)pt.getActualTypeArguments()[0];
						}
						if (cc.equals(clazz)){
							String fkName = mJoin.get(clazz).getFk();
							String preffix =joinTable+joinCounter+"_"; 
							joinCounter++;
							AppEntitiesConteiner appEc = AppSimpleUtil.getAppEntitiesConteiner();
							EntityContainer eContOrig = appEc.getEntity(clazz);
							if (eContOrig == null){
								if (appEc.addEntity(clazz)){
									eContOrig = appEc.getEntity(clazz);
								}
							}
							final EntityContainer eCont = eContOrig; 
							mappedById.forEach((key, val)->{
								Object joinObj = null;
								List<Object> joinList = null;
								Map<Object, Object> mappedJoinById = null;
								try {
									mappedJoinById = parseResultSetRefJoin(clazz, eCont.getFields(), hm.stream().filter(X->X.get(fkName).equals(key)).collect(Collectors.toList()), eCont.getIdentFieldName(), preffix);
									if (mappedJoinById != null && !mappedJoinById.isEmpty()) joinList = new ArrayList<>(mappedJoinById.values());
									if (joinMode == 1){
										fi.set(val, joinList);
									}else{
										if (joinList!=null && !joinList.isEmpty()){
											joinObj = joinList.get(0);
											fi.set(val, joinObj);
										}
									}
								} catch (Exception e) {
									logger.error("error panic :{}", e);
								}
							});
						}
					}
				}
			}
		}
		return result;
	}
}
