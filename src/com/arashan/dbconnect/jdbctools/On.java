package com.arashan.dbconnect.jdbctools;

/**
 * On is class for binding
 * classes in join relationship
 * @author luger
 *
 */
public class On {
	public String fk = "";
	public String pk = "";
	public IQueryBuilder qBuilder;
	public int joinMode = 0; //0 == unset, 1 == one to many, 2 == one to one
	public String relativeFieldName = "";
	public On setFk (String fk){this.fk = fk; return this;}
	public On setPk (String pk){this.pk = pk; return this;}
	public On setQuery (IQueryBuilder qBuilder){this.qBuilder = qBuilder; return this;}
	public On setJoinMode (int jm){this.joinMode = jm; return this;}
	public On setRelativeFieldName (String relField){this.relativeFieldName = relField; return this;}
}
