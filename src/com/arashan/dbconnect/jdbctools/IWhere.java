package com.arashan.dbconnect.jdbctools;

import java.util.List;

public interface IWhere {
	public IWhere and (WhereClause wc);
	public IWhere or (WhereClause wc);
	public IWhere and (IWhere wc);
	public IWhere or (IWhere wc);
	public String build ();
	public List<Operand> getStack ();
}
