package com.arashan.dbconnect.jdbctools;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arashan.dbconnect.jdbctools.listeners.AppSimpleUtil;

/**
 * QueryBuilder is class for generating
 * SQL scripts for select, update, insert DATA
 * realized in SDL form
 * @author luger
 *
 */
public class QueryBuilder implements IQueryBuilder{
	private final Logger logger = LoggerFactory.getLogger(QueryBuilder.class);
	/*
	 * main EntityContainer
	 */
	protected EntityContainer eCont = null;
	protected List<On> joinBuilders = new LinkedList<On>();
	protected List<IQueryBuilder> unionBuilders = new LinkedList<IQueryBuilder>();
	protected List<IQueryBuilder> unionAllBuilders = new LinkedList<IQueryBuilder>();
	protected IWhere where = null;
	/**
	 * queryMode:
	 * 0 - not set
	 * 1 - select
	 * 2 - with select
	 * 3 - insert
	 * 4 - update
	 * 5 - delete
	 * 6 - ?:) - someThing else
	 */
	protected int queryMode = 0;

	/**
	 * only for DDL scripts, if inserting or updating queris like 'insert (...) select (...)'
	 */
	protected int ddlSelectMode = 0;
	
	/**
	 * count :
	 * 0 - default select
	 * 1 - get count of rows in select
	 */
	protected int count = 0;
	protected String resultQuery = "";
	/**
	 * if joinmode equals 0 then joins get from joinBuilders
	 * else joins get from Class structure
	 */
	protected int joinMode = 0;
	/**
	 * list of Classes those can be selected
	 * if null = all composed classes will 
	 * join
	 */
	protected List<Class<?>> joinsRestrictions = null;
	protected List<Class<?>> rejectedJoinClasses = null;
	private List<String> exceptFields = new ArrayList<String>();
	Class<?> ownClass = null;
	/**
	 * default constructor
	 */
	protected QueryBuilder(){}
	
	/**
	 * constructor for Entity Container 
	 */
	protected QueryBuilder(EntityContainer eCont){
		this.eCont = eCont;
	}
	
	protected QueryBuilder(Class<?> c){
		ownClass = c;
		AppEntitiesConteiner appEc = AppSimpleUtil.getAppEntitiesConteiner();
		eCont = appEc.getEntity(c);
		if (eCont == null){
			if (appEc.addEntity(c)){
				eCont = appEc.getEntity(c);
			}
		}
	}

	public static QueryBuilder createBuilder (EntityContainer eCont){
		return new QueryBuilder(eCont);
	}
	
	public static QueryBuilder createBuilder (Class<?> c){
		return new QueryBuilder(c);
	}
	
	@Override
	public String getTbName(){
		return this.eCont.tableName;
	}
	
	@Override
	public int getQueryMode (){
		return this.queryMode;
	}
	
	@Override
	public String getResultQuery (){
		return this.resultQuery;
	}
	
	@Override
	public QueryBuilder select (){
		if (queryMode == 0) queryMode = 1;
		else ddlSelectMode = 1;
		return this;
	}
	
	@Override
	public QueryBuilder update (){
		queryMode = 4;
		return this;
	}

	@Override
	public QueryBuilder insert (){
		queryMode = 3;
		return this;
	}

	@Override
	public QueryBuilder delete (){
		queryMode = 5;
		return this;
	}
	
	@Deprecated
	@Override
	public QueryBuilder join (String tableName) throws QueryMethodException{
		if (queryMode != 1) throw new QueryMethodException("join method cannot be assigned for update,delete,insert mode");
		return this;
	}

	@Deprecated
	@Override
	public QueryBuilder join (EntityContainer joinCont, String pk, String fk) throws QueryMethodException{
		if (queryMode != 1) throw new QueryMethodException("join method cannot be assigned for update,delete,insert mode");
		return this;
	}

	@Override
	public QueryBuilder join (Class<?> c, String pk, String fk) throws QueryMethodException{
		if (queryMode != 1) throw new QueryMethodException("join method cannot be assigned for update,delete,insert mode");
		joinBuilders.add(new On().setPk(pk).setFk(fk).setQuery(new QueryBuilder (c)).setJoinMode(1));
		return this;
	}
	
	@Override
	public QueryBuilder join (Class<?> c) throws QueryMethodException{
		if (queryMode != 1) throw new QueryMethodException("join method cannot be assigned for update,delete,insert mode");
		AppEntitiesConteiner appEc = AppSimpleUtil.getAppEntitiesConteiner();
		EntityContainer eCont = appEc.getEntity(c);
		if (eCont == null){
			if (appEc.addEntity(c)){
				eCont = appEc.getEntity(c);
			}
		}
		Map<Class<?>, Join> mJoin = this.eCont.getJoinFields().get(eCont.getTableName());
		Join join = mJoin.get(c);
		logger.info("....."+join);
//		if (join == null){
//			joinBuilders.forEach(c->c.);
//		}
		if (join == null)throw new QueryMethodException("impossible call join method, join info is Empty");
		joinBuilders.add(new On().setPk(join.getPk()).setFk(join.getFk()).setQuery(new QueryBuilder (c)).setJoinMode(join.joinMode).setRelativeFieldName(join.relativeFieldName));
		return this;
	}

	@Override
	public IQueryBuilder join(IQueryBuilder qb, String pk, String fk) throws QueryMethodException {
		if (qb.getQueryMode()!=1)throw new QueryMethodException("join method cannot be assigned for update,delete,insert mode");
		if (queryMode != 1) throw new QueryMethodException("join method cannot be assigned for update,delete,insert mode");
		
		joinBuilders.add(new On().setPk(pk).setFk(fk).setQuery(qb).setJoinMode(1));
		return this;
	}
	
	@Override
	public QueryBuilder union(IQueryBuilder qb) {
		if (qb.getQueryMode()!=1)throw new QueryMethodException("union method cannot be assigned for update,delete,insert mode");
		if (queryMode != 1) throw new QueryMethodException("union method cannot be assigned for update,delete,insert mode");
		unionBuilders.add(qb);
		return this;
	}
	
	@Override
	public QueryBuilder unionAll (IQueryBuilder qb){
		if (qb.getQueryMode()!=1)throw new QueryMethodException("unionAll method cannot be assigned for update,delete,insert mode");
		if (queryMode != 1) throw new QueryMethodException("unionAll method cannot be assigned for update,delete,insert mode");
		unionAllBuilders.add(qb);
		return this;
	}

	private void buildJoins(){
		Map<String, Map<Class<?>, Join>> joinMap = this.eCont.getJoinFields();
		List<java.lang.reflect.Field> joinFields = this.eCont.getJoinFieldList();
		if (joinMap != null){
			for (java.lang.reflect.Field fieldkey:joinFields){
				String key = fieldkey.getName();
				Map<Class<?>, Join> mJoin = joinMap.get(key);
				for (Class<?> clazz:mJoin.keySet()){
					if (joinsRestrictions != null){
						boolean found = joinsRestrictions.stream().filter(X->X.getName().equalsIgnoreCase(clazz.getName())).findFirst().isPresent();
						if (!found)continue;
					}
					if (rejectedJoinClasses != null){
						boolean found = rejectedJoinClasses.stream().filter(X->X.getName().equalsIgnoreCase(clazz.getName())).findFirst().isPresent();
						if (found)continue;
					}
					Join join = mJoin.get(clazz);
					logger.info("buildJoins....."+join);
					if (join == null)throw new QueryMethodException("impossible call join method, join info is Empty");
					joinBuilders.add(new On().setPk(join.getPk()).setFk(join.getFk()).setQuery(new QueryBuilder (clazz)).setJoinMode(join.joinMode).setRelativeFieldName(join.relativeFieldName));
				}
			}
		}
//		logger.info("....."+join);
	}
	
	private String buildSelect (){
		StringBuffer sbMain = new StringBuffer();
		StringBuffer sbJoins = new StringBuffer();
		int counter = 0; 
		String mainTable = this.eCont.getTableName();
		sbMain.append("SELECT ");
		boolean hasExcepts = !exceptFields.isEmpty();
		if (count == 0){
			// adding own fields
			for (String field:this.eCont.fieldNames){
				if (hasExcepts){
					if (exceptFields.stream().filter(x->x.equalsIgnoreCase(field)).findFirst().isPresent())continue;
				}
				if (counter > 0)sbMain.append(",");
				sbMain.append(mainTable+"."+field+" as "+field);
				counter ++;
			}
		}
		if (joinMode == 1){
			buildJoins ();
		}
		// adding join fields
		int joinTableCounter = 0;
		for (On on:this.joinBuilders){
			if (on.joinMode == 1 || on.joinMode == 2){
				String joinTable = on.qBuilder.getTbName();
				/**
				 * small hack : set JOIN to LEFT JOIN only
				 * TODO it must be replaced
				 */
				sbJoins.append("LEFT JOIN ("+on.qBuilder.setExceptFields(exceptFields).select ().build()+") ");
				sbJoins.append(joinTable+joinTableCounter);
				sbJoins.append(" ON "+mainTable+"."+on.pk);
				sbJoins.append("=");
				sbJoins.append(joinTable+joinTableCounter+".");
				sbJoins.append(on.fk+" ");
				// adding own fields
				if (count == 0){
					for (String field:on.qBuilder.getFieldsName()){
						if (counter > 0)sbMain.append(",");
						sbMain.append(joinTable);
						sbMain.append(joinTableCounter+"."+field+" ");
						sbMain.append(joinTable+joinTableCounter+"_"+field);
						counter ++;
					}
				}
				joinTableCounter ++;
				// adding join fields
			}else if(on.joinMode == 3){
				
			}
		}
		// only fo count rows 
		if (count == 1){
			sbMain.append(" COUNT(1) AS __ROWS_COUNT ");
		}
		//this.where.And();
		sbMain.append(" FROM ");
		sbMain.append(this.eCont.schema.isEmpty()?"":this.eCont.schema+".");
		sbMain.append(mainTable);
		sbMain.append(" ");
		sbMain.append(sbJoins.toString());
		sbMain.append(new WhereBuilder(this.where, joinBuilders, ownClass.getSimpleName(), mainTable).build());
		return sbMain.toString();
	}

	private String buildInsert (){
		StringBuffer sbMain = new StringBuffer();
		int counter = 0; 
		String mainTable = this.eCont.getTableName();
		sbMain.append("INSERT INTO ");
		sbMain.append(this.eCont.schema.isEmpty()?"":this.eCont.schema+".");
		sbMain.append(mainTable);
		sbMain.append(" ( ");
		// adding own fields
		String prepareParams = "";
		boolean hasExcepts = !exceptFields.isEmpty();
		for (String field:this.eCont.fieldNames){
			if (hasExcepts){
				if (exceptFields.stream().filter(x->x.equalsIgnoreCase(field)).findFirst().isPresent())continue;
			}
			if (field.equalsIgnoreCase(eCont.getIdentFieldName()) && eCont.isIdentIsAutoGenerated()) continue;
			if (counter > 0){
				sbMain.append(",");
				prepareParams += ",";
			}
			sbMain.append(field);
			prepareParams += "?";
			counter ++;
		}
		sbMain.append(" ) values (");
		sbMain.append(prepareParams);
		sbMain.append(")");
		return sbMain.toString();
	}
	
	private String buildUpdate (){
		StringBuffer sbMain = new StringBuffer();
		int counter = 0; 
		String mainTable = this.eCont.getTableName();
		sbMain.append("UPDATE ");
		sbMain.append(this.eCont.schema.isEmpty()?"":this.eCont.schema+".");
		sbMain.append(mainTable);
		sbMain.append(" SET ");
		// adding own fields
		boolean hasExcepts = !exceptFields.isEmpty();
		for (String field:this.eCont.fieldNames){
			if (hasExcepts){
				if (exceptFields.stream().filter(x->x.equalsIgnoreCase(field)).findFirst().isPresent())continue;
			}
			if (field.equalsIgnoreCase(eCont.getIdentFieldName()) && eCont.isIdentIsAutoGenerated()) continue;
			if (counter > 0){
				sbMain.append(",");
			}
			sbMain.append(field);
			sbMain.append("=?");
			counter ++;
		}
		return sbMain.toString();
	}
	
	@Override
	public String build (){
		switch (this.queryMode){
		case 0:
			throw new QueryMethodException("unable build Query");
		case 1:
			this.resultQuery = buildSelect ();
			break;
		case 3:	
			this.resultQuery = buildInsert ();
			break;
		case 4:	
			this.resultQuery = buildUpdate ();
			break;
		}
		return this.resultQuery;
	}

	@Override
	public IQueryBuilder where(IWhere clause) {
		this.where = clause;
		return this;
	}

	@Override
	public String getSchema() {
		return eCont.schema;
	}

	@Override
	public List<String> getFieldsName() {
		return this.eCont.fieldNames;
	}

	@Override
	public IQueryBuilder count() {
		this.count = 1;
		return this;
	}

	@Override
	public IQueryBuilder joinMode(int joinMode) {
		this.joinMode = joinMode;
		return this;
	}

	@Override
	public IQueryBuilder joinRestrictionClasses(List<Class<?>> joinsRestrictions) {
		if (joinMode == 1) this.joinsRestrictions = joinsRestrictions;
		return this;
	}
	 
	@Override
	public IQueryBuilder setAcceptedJoinClasses(List<Class<?>> joinsRestrictions) {
		return this.joinRestrictionClasses(joinsRestrictions);
	}

	@Override
	public IQueryBuilder setAcceptedJoinClasses(Class<?>... joinsRestrictions) {
		return this.joinRestrictionClasses(Arrays.asList(joinsRestrictions));
	}

	@Override
	public IQueryBuilder setRejectedJoinClasses(List<Class<?>> rejectedJoinClasses) {
		if (joinMode == 1) this.rejectedJoinClasses = rejectedJoinClasses;
		return this;
	}

	@Override
	public IQueryBuilder setRejectedJoinClasses(Class<?>... rejectedJoinClasses) {
		return setRejectedJoinClasses(Arrays.asList(rejectedJoinClasses));
	}

	@Override
	public IQueryBuilder setExceptFields(List<String> exceptFields) {
		this.exceptFields  = exceptFields;
		return this;
	}

	@Override
	public <T> List<T> fetch(DBconnector conn) {
		if (conn.isAccessed()){
			List<Object> params = new ArrayList<Object>();
			params = this.where.getStack().stream().filter(x->x.getMode() != 1).map(x->x.getWc().getEqObject()).collect(Collectors.toList());
			try {
					return conn.runBuilder(this, params);
			} catch (Exception e) {
				logger.error("error:{}", e);
			}
		}
		return new ArrayList<T>();
	}

	@Override
	public String getOwnClassName() {
		return this.ownClass.getSimpleName();
	}

	@Override
	public Class<?> getOwnClass() {
		return ownClass;
	}

	@Override
	public EntityContainer getECont() {
		return eCont;
	}

	@Override
	public List<On> getJoinBuilders() {
		return joinBuilders;
	}
}