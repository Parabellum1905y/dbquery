package com.arashan.dbconnect.jdbctools;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WFDBconnector extends DBconnector{
	private static final Logger logger = LoggerFactory.getLogger(WFDBconnector.class);

	private WFDBconnector (boolean constConnected){
		super (true);
	}
	
	private WFDBconnector (boolean constConnected, String jndi_name){
		super (true, jndi_name);
	}

	public WFDBconnector (String jndi_name){
		super (jndi_name);
	}

	public WFDBconnector (){
		super ();
	}
	
	@Override
	final protected Connection createConnection() throws SQLException {
		logger.info ("Creating instance connection, jndi = {}",jndi_name);
		javax.sql.DataSource ds  =null;
		try{
			InitialContext context = new InitialContext();
			ds = (javax.sql.DataSource) context.lookup(this.jndi_name);
		}
		catch(NamingException ne){
			logger.error("exception in Connection.getWFConnection");
			ne.printStackTrace();
		}
	//return ds.getConnection(user,password);
		return ds.getConnection();
	}

	public static DBconnector getPermConnection (){
		return new WFDBconnector(true);
	}

	public static DBconnector getPermConnection (String jndi_name){
		return new WFDBconnector(true, jndi_name);
	}

	@Override
	protected void closeConnection(Connection conn) {
		try{ 
			if(conn!=null) 
				conn.close();
		}
		catch(SQLException e){
			logger.error("handled exception on close : "+ e.getMessage());
		}
	}
}
