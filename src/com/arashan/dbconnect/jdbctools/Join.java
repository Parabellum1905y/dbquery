package com.arashan.dbconnect.jdbctools;

public class Join {
	public String fk = "";
	public String pk = "";
	public String tb = "";
	public String relativeFieldName = "";
	public int joinMode = 0; //0 == unset, 1 == one to many, 2 == one to one
	
	public String getTb() {
		return tb;
	}
	public void setTb(String tb) {
		this.tb = tb.toUpperCase();
	}
	public void setFk (String fk){this.fk = fk.toUpperCase();}
	public String getFk() {
		return fk;
	}
	public void setPk (String pk){this.pk = pk.toUpperCase();}
	public String getPk() {
		return pk;
	}
	
	public int getJoinMode() {
		return joinMode;
	}
	public void setJoinMode(int joinMode) {
		this.joinMode = joinMode;
	}
	public void setRelativeFieldName(String relativeFieldName) {
		this.relativeFieldName = relativeFieldName;
	}
	@Override
	public String toString (){
		return "PK:"+this.pk+";FK:"+this.fk+";Ref Table:"+this.tb+"; join mode : "+this.joinMode+";field:"+this.relativeFieldName;
	}
}
