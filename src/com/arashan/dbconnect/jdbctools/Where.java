package com.arashan.dbconnect.jdbctools;

import java.util.ArrayList;
import java.util.List;

enum Condition{
	and,
	or,
	lb,
	rb
}

class Operand{
	int mode = 0;// by default is operand, 1 - is operator
	Condition cond = Condition.or;
	WhereClause wc = null;
	
	public Operand (WhereClause wc){
		this.wc = wc;
	}
	
	public Operand (Condition cond){
		mode = 1;
		this.cond = cond;
	}

	public int getMode() {
		return mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public Condition getCond() {
		return cond;
	}

	public void setCond(Condition cond) {
		this.cond = cond;
	}

	public WhereClause getWc() {
		return wc;
	}

	public void setWc(WhereClause wc) {
		this.wc = wc;
	}
}

public class Where implements IWhere {
	List<Operand> fullStack = new ArrayList<Operand>();
	
	protected Where (WhereClause wc){
		fullStack.add(new Operand(wc));
	}
	
	public static Where where (WhereClause wc){
		return new Where (wc);
	}

	@Override
	public List<Operand> getStack() {
		return fullStack;
	}

	@Override
	public IWhere and(WhereClause wc) {
		fullStack.add(new Operand(Condition.and));
		fullStack.add(new Operand(wc));
		return this;
	}

	@Override
	public IWhere or(WhereClause wc) {
		fullStack.add(new Operand(Condition.or));
		fullStack.add(new Operand(wc));
		return this;
	}

	@Override
	public IWhere and(IWhere wc) {
		fullStack.add(new Operand(Condition.and));
		fullStack.add(new Operand(Condition.lb));
		fullStack.addAll(wc.getStack());
		fullStack.add(new Operand(Condition.rb));
		return this;
	}

	@Override
	public IWhere or(IWhere wc) {
		fullStack.add(new Operand(Condition.or));
		fullStack.add(new Operand(Condition.lb));
		fullStack.addAll(wc.getStack());
		fullStack.add(new Operand(Condition.rb));
		return this;
	}

	@Override
	public String build() {
		
		return null;
	}
}