package com.arashan.dbconnect.jdbctools.listeners;

import com.arashan.dbconnect.jdbctools.AppEntitiesConteiner;

/**
 * util for singleton storing
 * mapping entities fields , annotations and other
 * @author Anvar
 * @version 1.0
 */
public class AppSimpleUtil {
	public static AppEntitiesConteiner aec;
	public static String globalJNDI = "";
	
	public static AppEntitiesConteiner getAppEntitiesConteiner (){
		return aec;
	}

	public static void setAppEntitiesConteiner (AppEntitiesConteiner ae){
		aec = ae;
	}
	
	public static String getGlobalJNDI () {
		return globalJNDI;
	}
	
	public static void setGlobalJNDI (String jndi) {
		globalJNDI = jndi;
	}
}
