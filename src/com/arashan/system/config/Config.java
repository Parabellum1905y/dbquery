package com.arashan.system.config;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
* 
* @author anvar
* @version 1.0
* Класс для загрузки конфигурациооных данных из файла конфигурации.<br> 
*/
public class Config{
	private final Logger logger = LoggerFactory.getLogger(Config.class);
	private Properties props;
	/** 
	 * 
	 * @param propFile файл конфигурации
	 * @throws FileNotFoundException файл не найден
	 * @throws IOException ошибка ввода\вывода
	 * @throws IllegalArgumentException Неверный аргумент
	 * @throws ClassNotFoundException не найден класс отправителя\получателя
	 */
	public Config(String propFile) throws FileNotFoundException, IOException, IllegalArgumentException,
	        ClassNotFoundException {
	    //prepare the properties object
	    props = new Properties();
	    //get an input stream
	    FileInputStream fin = new FileInputStream(propFile);
	    //load properties from the stream
	    props.load(fin);
	    //close the input stream
	    fin.close();
	}

	public Config(InputStream is) throws FileNotFoundException, IOException, IllegalArgumentException,
		        ClassNotFoundException {
	    props = new Properties();
	    props.load(is);
	    is.close();
	}
	
	public String getProp (String name){
		return (String) this.props.get(name);
	}
	
	public Properties getProps (){
		return this.props;
	}
	/**
	 * system summary.
	 *
	 */
	public String getConfigSummary(){
	    //the system property key        
	    String key = null;
	    String mes = "";
	    //loop through each system property and write to the system log
	    Set<Object> ks = System.getProperties().keySet();
	    for (Iterator<Object> i = ks.iterator(); i.hasNext();){
	        key = (String) i.next();
	        mes += (key + " = " + System.getProperties().getProperty(key))+"\n";
	    }
	    return mes;
	}
}