package com.arashan.system.config;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoadConfig {
	private static final Logger logger = LoggerFactory.getLogger(LoadConfig.class);
	public static Map<String, Config> configs = new HashMap<String, Config>();
	
	public static Config loadConfig (String name){
		Config conf = null;
		if (configs.get(name) !=null){
			conf = configs.get(name);
			logger.info("config already loaded; {}", name);
		}else{
			try {
				conf = new Config (new LoadConfig().getClass().getClassLoader().getResourceAsStream(name));
				configs.put(name, conf);
				logger.info("new config loaded; {}", name);
			} catch (IllegalArgumentException | ClassNotFoundException
					| IOException e) {
				logger.error ("error in loading :{}",e);
			}
		}
		return conf;
	}

	public static Config reloadConfig (String name){
		Config conf = null;
		try {
			conf = new Config (new LoadConfig().getClass().getClassLoader().getResourceAsStream(name));
			configs.put(name, conf);
			logger.info("new config loaded; {}", conf.getProps());
		} catch (IllegalArgumentException | ClassNotFoundException
				| IOException e) {
			logger.error ("error in loading :{}",e);
		}
		return conf;
	}
}
